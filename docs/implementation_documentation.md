# Implementační dokumentace modulu _microclimate_

## Záměr

Modul slouží k ukládání a poskytování informací o microclimatu.

## Vstupní data

### Stahujeme data ze dvou zdrojů, zpracováváme je a ukládáme do samostatných tabulek.

#### _Protected endpoint at https://api.agdata.cz/sensors/devices_

-   zdroj dat
    -   url: [config.datasources.MicroclimateAgdataCity](https://api.agdata.cz/sensors)
    -   apiKey: config.datasources.MicroclimateAgdataCityApikey
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [sensorDevicesDatasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/src/integration-engine/datasources/SensorDevicesDataSourceFactory.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/test/integration-engine/data/sensor-devices-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.microclimate.refreshSensorDevices
            -   rabin `0 0 7 * * *`
            -   prod `0 0 7 * * *`
-   název rabbitmq fronty
    -   dataplatform.microclimate.refreshSensorDevices

#### _Protected endpoint at https://api.agdata.cz/sensors?sensorAddr={param1}&dateRange={param2}_

-   zdroj dat
    -   url: [config.datasources.MicroclimateAgdataCity](https://api.agdata.cz/sensors)
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [measurementsDatasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/src/integration-engine/datasources/MeasurementsDataSourceFactory.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/test/integration-engine/data/measurements-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.microclimate.refreshMeasurements
            -   rabin `0 */10 * * * *`
            -   prod `0 */10 * * * *`
-   název rabbitmq fronty
    -   dataplatform.microclimate.refreshMeasurements

### Microclimate

#### _task: RefreshSensorDevicesTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.microclimate.refreshSensorDevices
    -   bez parametrů
-   datové zdroje
    -   SensorDevicesDataSourceFactory
-   transformace
    -   [SensorDevicesTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/src/integration-engine/transformations/SensorDevicesTransformation.ts) - mapování pro `SensorDevicesRepository`

#### _task: RefreshMeasurementsTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.microclimate.refreshMeasurements
-   datové zdroje
    -   SensorDevicesRepository ('sensorAddr' pro message na závislé fronty)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.microclimate.refreshMeasurementsById
    -   název: dataplatform.microclimate.refreshSensorViews

#### _task: RefreshMeasurementsByIdTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.microclimate.refreshMeasurementsById
-   datové zdroje
    -   task: RefreshMeasurementsTask + MeasurementsDataSourceFactory
-   transformace
    -   [SensorDevicesTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/src/integration-engine/transformations/SensorDevicesTransformation.ts) - mapování pro `MeasurementsRepository`

#### _task: RefreshSensorViewsTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.microclimate.refreshSensorViews
    -   TTL: 10 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   žádné
-   transformace
    -   žádné
-   data modely
    -   SensorPointsHeightsViewModel `v_sensor_points_heights`
    -   SensorMeasurementsViewModel `v_sensor_measurements`
    -   SensorMeasurementsLatestViewModel `v_sensor_measurements_latest`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![microclimate er diagram](./assets/microclimate_erd.png)
    -   ![microclimate measurements views er diagram](./assets/microclimate_measure_views_erd.png)

# Popis Integraci datovych sad podle initial issue

### Tabulka `microclimate.sensor_devices` (tj. Vysílače):

<details>
<summary>API Request</summary>

apiKey: config.datasources.MicroclimateAgdataCityApikey

```
GET "https://api.agdata.cz/sensors/devices" -H "Authorization: Bearer <api-key>"

Header:
   Authorization: Bearer  (apiKey)

Host: https://api.agdata.cz
```

</details>

<details>
<summary>JSON Response</summary>

```json
{
    "status": "200",
    "statusText": "OK",
    "data": [
        {
            "name": "Tržnice | střed | VO | 1m | #063",
            "address": "323434316F317A18",
            "location": {
                "lat": 50.0989344,
                "lng": 14.4450222
            }
        },
        {
            "name": "Tržnice | střed | VO | 2m | 3m | #190",
            "address": "323434317F317F18",
            "location": {
                "lng": 14.4450222,
                "lat": 50.0989344
            }
        }
    ]
}
```

Poznámka: Pořadí proměnných zeměpisné délky a šířky je občas ve výstupu prohozeno.

</details>

<details>
<summary>Atributy microclimate_sensor_devices</summary>

| Agdata Atribut | Náš Atribut | Náš datový typ | Nullable? | Popis                                                      | Příklad                              | Poznámka                                                          |
| -------------- | ----------- | -------------- | --------- | ---------------------------------------------------------- | ------------------------------------ | ----------------------------------------------------------------- |
| name           | whole_name  | string         | Nope      | Adresa vysílače vč. umístění a polohy jednotlivých senzorů | Tržnice \| střed \| VO \| 1m \| #063 | Viz níže                                                          |
| address        | sensor_id   | string         | Nope      | Unikátní id vysílače                                       | 323434316F317A18                     | Primary key. Vstupuje do požadavku na volání jednotlivých měření. |
| lat            | lat         | float          | Nope      | zeměpisná šířka                                            | 50.0989344                           | -                                                                 |
| lng            | lng         | float          | Nope      | zeměpisná délka                                            | 14.4450222                           | -                                                                 |

**Poznámky:**

-   _Nullable_ options: "Jop" = NULL hodnoty jsou přípustné/mohou se objevit, "Nope" = přesně naopak
-   Do budoucna by se atribut `whole_name` mohl rozdělit na více sloupců (řešíme změnu a strojovou čitelnost ze strany Agdata)

</details>

### Tabulka `microclimate.measurements` (tj. Měření):

<details>
<summary>API Request</summary>

apiKey: config.datasources.MicroclimateAgdataCityApikey

```
GET "https://api.agdata.cz/sensors?sensorAddr={param1}&dateRange={param2}" -H "Authorization: Bearer <api-key>"

Header:
   Authorization: Bearer  (apiKey)

Host: https://api.agdata.cz
```

#### Query Parameters:

-   `{param1}: sensorAddr` (_Required_) = představuje unikátní identifikátor vysílače. Ta jsou k dispozici v první tabulce, proměnná `address`. Celkem jsou zatím k dispozici měření ze 44 různých vysílačů (portfolio se bude do konce roku 2022 rozšiřovat).

-   `{param2}: date` (_Optional_) = časové období. Lze zadat dvěma způsoby. Buďto je možné zadat zvolené časové období pomocí počátečního a koncového data:

| Parameter  | Type   | Description                                            |
| ---------- | ------ | ------------------------------------------------------ |
| sensorAddr | string | Unikátní adresa vysílače v podobě id. Povinný parametr |
| dateFrom   | string | Filtruje časové období, formát ISO 8601                |
| dateTo     | string | Filtruje časové období, formát ISO 8601                |

**Například:**

```
GET "https://api.agdata.cz/sensors?sensorAddr=323434317F317F18&dateFrom=ISODate&dateTo=ISODate" -H "Authorization: Bearer <api-key>"
```

`{param2}: dateRange` (_Optional_):

| Parameter | Type   | Description                                                                                                                        |
| --------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------- |
| dateRange | string | Časové období: "today", "yesterday", "currentWeek", "previousWeek", "currentMonth", "previousMonth", "currentYear", "previousYear" |

**Například:**

```
GET "https://api.agdata.cz/sensors?sensorAddr=323434317F317F18&dateRange=currentYear" -H "Authorization: Bearer <api-key>"

```

</details>

<details>
<summary>JSON Response</summary>

```json
{
    "status": "200",
    "statusText": "OK",
    "data": [
        {
            "id": "62e7a36bc1152d67c92a9c3c",
            "date": "2021-04-01T00:00:00.000Z",
            "sensor": {
                "type": "unitlv"
            },
            "data": {
                "temp1": 5.698664741970408,
                "maxTemp1": 22.4,
                "minTemp1": -5.8,
                "hum1": 75.22095513051848,
                "maxHum1": 100,
                "windDir": "N",
                "windSpeed": 4.317767154430699,
                "maxWindSpeed": 22.029718537839994,
                "windImpact": 34.56,
                "avgWindImpact": 8.165427643450018,
                "rainVolume": 39.55
            }
        }
    ]
}
```

**Poznámky:**

-   Samotné proměnné a jejich pořadí není pevně dané a liší se napříč vysílači, viz [Dokumentace] (https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/blob/master/datasets/Microclimate%20measurements.md#m%C4%9B%C5%99en%C3%AD-microclimate_measurements).

</details>

<details>
<summary>Atributy microclimate_measurements</summary>

Proměnné zaslané vysílači se liší, tj. různé vysílače odesílají různá měření/proměnné. Výskyt NULL hodnot je tedy možný (vše se integruje do jedné tabulky).

| Agdata Atribut | Náš Atribut    | Náš datový typ | Nullable | Popis                           | Příklad                  | Poznámka                                                                                         |
| -------------- | -------------- | -------------- | -------- | ------------------------------- | ------------------------ | ------------------------------------------------------------------------------------------------ |
| sensor_Addr    | sensor_id      | string         | Nope     | Unikátní id vysílače            | 3234343176318A18         | Povinný parametr requestu, není v response, foreign key na microclimate_sensor_devices.sensor_id |
| id             | measurement_id | string         | Nope     | Unikátní id jednotlivých měření | 62fb6460b13033fb7df6dd30 | Primary key                                                                                      |
| date           | measured_at    | timestamp      | Nope     | Datum a čas měření              | 2022-08-16T09:33:19.873Z | Ve formátu ISO 8601                                                                              |
| airTemp1       | air_temp       | float          | Jop      | Teplota vzduchu (ve °C)         | 26.4                     | -                                                                                                |
| airHum1        | air_hum        | float          | Jop      | Vlhkost vzduchu (v %)           | 40.9                     | -                                                                                                |
| pressure       | pressure       | integer        | Jop      | Atmosférický tlak (v Pa)        | 98600                    | -                                                                                                |
| windDir        | wind_dir       | string         | Jop      | Směr větru                      | N                        | -                                                                                                |
| windSpeed      | wind_speed     | float          | Jop      | Rychlost větru (v km/h)         | 2.3999760002399975       | -                                                                                                |
| windImpact     | wind_impact    | float          | Jop      | Náraz větru (v km/h)            | 7.2405                   | -                                                                                                |
| rainVolume     | precip         | float          | Jop      | Úhrn srážek (v mm)              | 39.55                    | Zatím v žádné z odpovědí                                                                         |
| irr            | sun_irr        | integer        | Jop      | Sluneční záření (v Lux)         | 8265                     | -                                                                                                |
| dendroCirc     | dendro_circ    | float          | Jop      | Obvod stromu (v mm)             | 416.75284830732494       | -                                                                                                |
| dendroGain     | dendro_gain    | float          | Jop      | TBD (v µm)                      | -33.247151692675075      | -                                                                                                |
| waterPot       | water_pot      | float          | Jop      | Vodní potenciál půdy (v kPa)    | -60.888                  | -                                                                                                |
| temp1          | soil_temp      | float          | Jop      | Teplota půdy (v °C)             | 12.5                     | -                                                                                                |

**Poznámky:**

-   Ze 44 nainstalovaných vysílačů jich zatím 27 vrací "prázdnou odpověď", resp. prázdnou datovou množinu (vysílače, mající pouze číslice v `sensorAddr` - můžu dodat seznam). Tuto odpověď nechceme mít v databázi (řešíme s dodavatelem). Response je tvaru:

```
{'status': 200, 'statusText': 'OK', 'data': []}
```

-   Tento typ odpovědi značí pouze chybějící měření (většinou v srpnu), tj. chceme mít zaintegrováno:

```
{'status': 200, 'statusText': 'OK', 'data': [{'sensor': {'type': 'unitlv'}, 'data': {}, 'id': '62f0ce19c1152d67c9362506', 'date': '2022-08-08T08:49:29.015Z', ...}
```

-   Seznam nemusí být nutně kompletní. Z vysílačů mající v id vysílače (`sensorAddr`) pouze číslice, např. "3234343178318310", získáváme prázdnou odpověď a nevíme tak zatím, jestli nebudou k dispozici další proměnné (vyřeší se během 2. fáze).

</details>

## Output API

### Obecné

- OpenAPI v3 dokumentace
    - [swagger] (https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/blob/development/docs/openapi.yaml).
- api je veřejné
- AsyncApi dokumentace
    - [AsyncApi](./asyncapi.yaml)

#### _/microclimate/locations_

- zdrojové tabulky
    - `sensor_devices_import`

#### _/microclimate/points_

- zdrojové tabulky
    - view `v_sensor_points` nad `sensor_devices_import`

#### _/microclimate/measurements_

- zdrojové tabulky
    - view `v_sensor_measurements` nad `sensor_devices_import` a `measurements`
