export interface ISensorDeviceImport {
    location_id: number;
    point_id: number;
    location: string;
    loc_description: string;
    loc_orientation: string;
    loc_surface: string;
    point_name: string;
    point_whole_name: string;
    address: string;
    sensor_position: string;
    sensor_position_detail: string;
    sensor_id: string;
    data_relevance: string;
    lat: number;
    lng: number;
    air_temp: number;
    air_hum: number;
    pressure: number;
    wind_dir: number;
    wind_impact: number;
    wind_speed: number;
    precip: number;
    sun_irr: number;
    soil_temp: number;
    water_pot: number;
    dendro_circ: number;
    dendro_gain: number;
}

export const sensorDevicesImport = {
    name: "SensorDevicesImport",
    pgTableName: "sensor_devices_import",
};
