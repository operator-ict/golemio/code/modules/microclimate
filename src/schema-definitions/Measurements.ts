export interface IMeasurements {
    sensor_id: string;
    measurement_id: string;
    measured_at: string;
    air_temp?: number;
    air_hum?: number;
    pressure?: number;
    wind_dir?: string;
    wind_speed?: number;
    wind_impact?: number;
    precip?: number;
    sun_irr?: number;
    dendro_circ?: number;
    dendro_gain?: number;
    water_pot?: number;
    soil_temp?: number;
}
export enum DateRange {
    TODAY = "today",
    YESTERDAY = "yesterday",
    CURRENTWEEK = "currentWeek",
    PREVIOUSWEEK = "previousWeek",
    CURRENTMONTH = "currentMonth",
    PREVIOUSMONTH = "previousMonth",
    CURRENTYEAR = "currentYear",
    PREVIOUSYEAR = "previousYear",
}

export interface IRefreshMeasurementsByIdTask {
    sensorAddr: string;
    dateRange: DateRange;

    // For future FROM-TO implementation:
    // dateFrom: string;
    // dateTo: string;
}

export const measurements = {
    name: "Measurements",
    pgTableName: "measurements",
};
