export interface ISensorMeasurementsView {
    sensor_id?: string;
    measured_at: Date | string;
    point_id: number;
    location_id: number;
    measure: string;
    value: number;
    unit: string;
}

export const sensorMeasurementsView = {
    name: "SensorMeasurementsView",
    pgTableName: "v_sensor_measurements",
};
