export interface ILatestMeasurementDto {
    point_id: number;
    measure: string;
    measured_at: Date;
}
