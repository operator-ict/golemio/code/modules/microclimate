import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ILatestMeasurementDto } from "./interfaces/ILatestMeasurementDto";

export class SensorMeasurementsLatestViewModel extends Model<ILatestMeasurementDto> implements ILatestMeasurementDto {
    declare point_id: number;
    declare measure: string;
    declare measured_at: Date;

    public static attributeModel: ModelAttributes<SensorMeasurementsLatestViewModel, ILatestMeasurementDto> = {
        point_id: { type: DataTypes.INTEGER },
        measure: { type: DataTypes.TEXT },
        measured_at: { type: DataTypes.DATE },
    };
}
