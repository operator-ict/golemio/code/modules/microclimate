import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorMeasurementsView } from "#sch/SensorMeasurementsView";

export class SensorMeasurementsViewModel extends Model<ISensorMeasurementsView> implements ISensorMeasurementsView {
    declare sensor_id: string;
    declare measured_at: Date;
    declare point_id: number;
    declare location_id: number;
    declare measure: string;
    declare value: number;
    declare unit: string;

    public static attributeModel: ModelAttributes<SensorMeasurementsViewModel, ISensorMeasurementsView> = {
        sensor_id: { type: DataTypes.STRING(50) },
        measured_at: { type: DataTypes.DATE },
        point_id: { type: DataTypes.INTEGER },
        location_id: { type: DataTypes.INTEGER },
        measure: { type: DataTypes.TEXT },
        value: { type: DataTypes.DOUBLE },
        unit: { type: DataTypes.TEXT },
    };
}
