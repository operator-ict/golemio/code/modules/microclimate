import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorPointsView } from "#sch/SensorPointsView";

export class SensorPointsViewModel extends Model<ISensorPointsView> implements ISensorPointsView {
    declare point_id: number;
    declare point_name: string;
    declare location_id: number;
    declare location: string;
    declare loc_description: string;
    declare loc_orientation: string;
    declare loc_surface: string;
    declare lat: number;
    declare lng: number;
    declare x_jtsk: null;
    declare y_jtsk: null;
    declare elevation_m: null;
    declare sensor_position: string;
    declare sensor_position_detail: string;
    declare measure: string;
    declare measure_cz: string;
    declare unit: string;

    public static attributeModel: ModelAttributes<SensorPointsViewModel, ISensorPointsView> = {
        point_id: { type: DataTypes.INTEGER },
        point_name: { type: DataTypes.STRING(50) },
        location_id: { type: DataTypes.INTEGER },
        location: { type: DataTypes.STRING(50) },
        loc_description: { type: DataTypes.STRING(50) },
        loc_orientation: { type: DataTypes.STRING(50) },
        loc_surface: { type: DataTypes.STRING(50) },
        lat: { type: DataTypes.REAL },
        lng: { type: DataTypes.REAL },
        x_jtsk: { type: DataTypes.INTEGER, allowNull: true },
        y_jtsk: { type: DataTypes.INTEGER, allowNull: true },
        elevation_m: { type: DataTypes.INTEGER, allowNull: true },
        sensor_position: { type: DataTypes.STRING(50) },
        sensor_position_detail: { type: DataTypes.STRING(50) },
        measure: { type: DataTypes.TEXT },
        measure_cz: { type: DataTypes.TEXT },
        unit: { type: DataTypes.TEXT },
    };
}
