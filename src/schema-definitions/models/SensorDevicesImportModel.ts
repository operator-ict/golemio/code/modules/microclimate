import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorDeviceImport } from "#sch/SensorDevicesImport";

export class SensorDevicesImportModel extends Model<ISensorDeviceImport> implements ISensorDeviceImport {
    declare location_id: number;
    declare point_id: number;
    declare location: string;
    declare loc_description: string;
    declare loc_orientation: string;
    declare loc_surface: string;
    declare point_name: string;
    declare point_whole_name: string;
    declare address: string;
    declare sensor_position: string;
    declare sensor_position_detail: string;
    declare sensor_id: string;
    declare data_relevance: string;
    declare lat: number;
    declare lng: number;
    declare air_temp: number;
    declare air_hum: number;
    declare pressure: number;
    declare wind_dir: number;
    declare wind_impact: number;
    declare wind_speed: number;
    declare precip: number;
    declare sun_irr: number;
    declare soil_temp: number;
    declare water_pot: number;
    declare dendro_circ: number;
    declare dendro_gain: number;

    public static attributeModel: ModelAttributes<SensorDevicesImportModel, ISensorDeviceImport> = {
        location_id: { type: DataTypes.INTEGER },
        point_id: { type: DataTypes.INTEGER },
        location: { type: DataTypes.STRING(50) },
        loc_description: { type: DataTypes.STRING(50) },
        loc_orientation: { type: DataTypes.STRING(50) },
        loc_surface: { type: DataTypes.STRING(50) },
        point_name: { type: DataTypes.STRING(50) },
        point_whole_name: { type: DataTypes.STRING(50) },
        address: { type: DataTypes.STRING(50) },
        sensor_position: { type: DataTypes.STRING(50) },
        sensor_position_detail: { type: DataTypes.STRING(50) },
        sensor_id: { type: DataTypes.STRING(50), primaryKey: true },
        data_relevance: { type: DataTypes.STRING(50) },
        lat: { type: DataTypes.REAL },
        lng: { type: DataTypes.REAL },
        air_temp: { type: DataTypes.INTEGER },
        air_hum: { type: DataTypes.INTEGER },
        pressure: { type: DataTypes.INTEGER },
        wind_dir: { type: DataTypes.INTEGER },
        wind_impact: { type: DataTypes.INTEGER },
        wind_speed: { type: DataTypes.INTEGER },
        precip: { type: DataTypes.INTEGER },
        sun_irr: { type: DataTypes.INTEGER },
        soil_temp: { type: DataTypes.INTEGER },
        water_pot: { type: DataTypes.INTEGER },
        dendro_circ: { type: DataTypes.INTEGER },
        dendro_gain: { type: DataTypes.INTEGER },
    };
}
