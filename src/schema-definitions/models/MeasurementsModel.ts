import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMeasurements } from "#sch/Measurements";

export class MeasurementsModel extends Model<IMeasurements> implements IMeasurements {
    declare sensor_id: string;
    declare measurement_id: string;
    declare measured_at: string;
    declare air_temp?: number | undefined;
    declare air_hum?: number | undefined;
    declare pressure?: number | undefined;
    declare wind_dir?: string | undefined;
    declare wind_speed?: number | undefined;
    declare wind_impact?: number | undefined;
    declare precip?: number | undefined;
    declare sun_irr?: number | undefined;
    declare dendro_circ?: number | undefined;
    declare dendro_gain?: number | undefined;
    declare water_pot?: number | undefined;
    declare soil_temp?: number | undefined;

    public static attributeModel: ModelAttributes<MeasurementsModel, IMeasurements> = {
        sensor_id: { type: DataTypes.STRING },
        measurement_id: { type: DataTypes.STRING, primaryKey: true },
        measured_at: { type: DataTypes.DATE },
        air_temp: { type: DataTypes.STRING },
        air_hum: { type: DataTypes.FLOAT },
        pressure: { type: DataTypes.INTEGER },
        wind_dir: { type: DataTypes.STRING },
        wind_speed: { type: DataTypes.FLOAT },
        wind_impact: { type: DataTypes.FLOAT },
        precip: { type: DataTypes.FLOAT },
        sun_irr: { type: DataTypes.INTEGER },
        dendro_circ: { type: DataTypes.FLOAT },
        dendro_gain: { type: DataTypes.FLOAT },
        water_pot: { type: DataTypes.FLOAT },
        soil_temp: { type: DataTypes.FLOAT },
    };

    public static jsonSchema: JSONSchemaType<IMeasurements[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                sensor_id: { type: "string" },
                measurement_id: { type: "string" },
                measured_at: { type: "string" },
                air_temp: { type: "number", nullable: true },
                air_hum: { type: "number", nullable: true },
                pressure: { type: "number", nullable: true },
                wind_dir: { type: "string", nullable: true },
                wind_speed: { type: "number", nullable: true },
                wind_impact: { type: "number", nullable: true },
                precip: { type: "number", nullable: true },
                sun_irr: { type: "number", nullable: true },
                dendro_circ: { type: "number", nullable: true },
                dendro_gain: { type: "number", nullable: true },
                water_pot: { type: "number", nullable: true },
                soil_temp: { type: "number", nullable: true },
            },
            required: ["sensor_id", "measurement_id", "measured_at"],
        },
    };
}
