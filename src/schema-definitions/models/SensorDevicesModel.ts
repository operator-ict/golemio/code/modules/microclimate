import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISensorDevice } from "#sch/SensorDevices";

export class SensorDevicesModel extends Model<ISensorDevice> implements ISensorDevice {
    declare whole_name: string;
    declare sensor_id: string;
    declare coordinate_lat?: number | undefined;
    declare coordinate_lng?: number | undefined;

    public static attributeModel: ModelAttributes<SensorDevicesModel, ISensorDevice> = {
        whole_name: { type: DataTypes.STRING },
        sensor_id: { type: DataTypes.STRING, primaryKey: true },
        coordinate_lat: { type: DataTypes.FLOAT },
        coordinate_lng: { type: DataTypes.FLOAT },
    };

    public static jsonSchema: JSONSchemaType<ISensorDevice[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                whole_name: { type: "string" },
                sensor_id: { type: "string" },
                coordinate_lat: { type: "number", nullable: true },
                coordinate_lng: { type: "number", nullable: true },
            },
            required: ["whole_name", "sensor_id"],
        },
    };
}
