export interface ISensorDevice {
    whole_name: string;
    sensor_id: string;
    coordinate_lat?: number;
    coordinate_lng?: number;
}

export const sensorDevices = {
    name: "SensorDevices",
    pgTableName: "sensor_devices",
};
