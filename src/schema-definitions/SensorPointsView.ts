export interface ISensorPointsView {
    point_id: number;
    point_name: string;
    location_id: number;
    location: string;
    loc_description: string;
    loc_orientation: string;
    loc_surface: string;
    lat: number;
    lng: number;
    x_jtsk: null;
    y_jtsk: null;
    elevation_m: null;
    sensor_position: string;
    sensor_position_detail: string;
    measure: string;
    measure_cz: string;
    unit: string;
}

export const sensorPointsView = {
    name: "SensorPointsView",
    pgTableName: "v_sensor_points",
};
