import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IDeviceLocationInput {
    lat?: number;
    lng?: number;
}

export interface IDeviceInput {
    name: string;
    address: string;
    location: IDeviceLocationInput;
}

const SensorDevicesJsonSchema: JSONSchemaType<IDeviceInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            name: { type: "string" },
            address: { type: "string" },
            location: {
                type: "object",
                properties: {
                    lat: { type: "number", nullable: true },
                    lng: { type: "number", nullable: true },
                },
            },
        },
        required: ["name", "address", "location"],
    },
    additionalProperties: false,
};

export const sensorDevicesDatasource: { name: string; jsonSchema: JSONSchemaType<IDeviceInput[]> } = {
    name: "SensorDevicesDatasource",
    jsonSchema: SensorDevicesJsonSchema,
};
