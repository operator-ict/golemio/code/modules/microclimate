import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IMeasurementInput {
    sensor: {
        type: string;
    };
    id: string;
    date: string;
    data: {
        airTemp1?: number;
        airHum1?: number;
        pressure?: number;
        windDir?: string;
        windSpeed?: number;
        windImpact?: number;
        rainVolume?: number;
        irr?: number;
        dendroCirc?: number;
        dendroGain?: number;
        waterPot?: number;
        temp1?: number;
    };
}

const MeasurementsJsonSchema: JSONSchemaType<IMeasurementInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            sensor: { type: "object", properties: { type: { type: "string" } }, required: ["type"] },
            id: { type: "string" },
            date: { type: "string" },
            data: {
                type: "object",
                properties: {
                    airTemp1: { type: "number", nullable: true },
                    airHum1: { type: "number", nullable: true },
                    pressure: { type: "number", nullable: true },
                    windDir: { type: "string", nullable: true },
                    windSpeed: { type: "number", nullable: true },
                    windImpact: { type: "number", nullable: true },
                    rainVolume: { type: "number", nullable: true },
                    irr: { type: "number", nullable: true },
                    dendroCirc: { type: "number", nullable: true },
                    dendroGain: { type: "number", nullable: true },
                    waterPot: { type: "number", nullable: true },
                    temp1: { type: "number", nullable: true },
                },
            },
        },
        required: ["sensor", "id", "date"],
    },
    additionalProperties: false,
};

export const measurementsDatasource: { name: string; jsonSchema: JSONSchemaType<IMeasurementInput[]> } = {
    name: "MeasurementsDatasource",
    jsonSchema: MeasurementsJsonSchema,
};
