import { measurements } from "#sch/Measurements";
import { sensorDevices } from "#sch/SensorDevices";
import { sensorDevicesImport } from "#sch/SensorDevicesImport";
import { sensorMeasurementsView } from "#sch/SensorMeasurementsView";
import { sensorPointsView } from "#sch/SensorPointsView";
import { measurementsDatasource } from "#sch/datasources/MeasurementsJsonSchema";
import { sensorDevicesDatasource } from "#sch/datasources/SensorDevicesJsonSchema";
import { sensorMeasurementsLatestView } from "./SensorMeasurementsLatestView";

const forExport: any = {
    name: "Microclimate",
    pgSchema: "microclimate",
    datasources: {
        sensorDevicesDatasource,
        measurementsDatasource,
    },
    definitions: {
        sensorDevices,
        measurements,
        sensorDevicesImport,
        sensorPointsView,
        sensorMeasurementsView,
        sensorMeasurementsLatestView,
    },
};

export { forExport as Microclimate };
