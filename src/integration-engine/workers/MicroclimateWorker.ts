import { MICROCLIMATE_WORKER_NAME } from "#ie/const";
import { MicroclimateContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshMeasurementsByIdTask, RefreshMeasurementsTask, RefreshSensorDevicesTask } from "./tasks";

export class MicroclimateWorker extends AbstractWorker {
    protected name = MICROCLIMATE_WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(MicroclimateContainer.resolve(ModuleContainerToken.RefreshSensorViewsTask));
        this.registerTask(new RefreshSensorDevicesTask(this.getQueuePrefix()));
        this.registerTask(new RefreshMeasurementsTask(this.getQueuePrefix()));
        this.registerTask(new RefreshMeasurementsByIdTask(this.getQueuePrefix()));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
