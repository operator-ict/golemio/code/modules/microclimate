import { SensorDevicesTransformation } from "#ie/transformations/SensorDevicesTransformation";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { SensorDevicesDataSourceFactory } from "#ie/datasources/SensorDevicesDataSourceFactory";
import { SensorDevicesRepository } from "#ie/repositories";

export class RefreshSensorDevicesTask extends AbstractEmptyTask {
    public readonly queueName = "refreshSensorDevices";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private dataSource: DataSource;
    private transformation: SensorDevicesTransformation;
    private repository: SensorDevicesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = SensorDevicesDataSourceFactory.getDataSource();
        this.transformation = new SensorDevicesTransformation();
        this.repository = new SensorDevicesRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.repository.saveBulk(transformedData);
    }
}
