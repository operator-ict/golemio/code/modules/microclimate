import { ORIGIN_HOSTNAME_HEADER } from "#ie/const";
import { MeasurementsDataSourceFactory } from "#ie/datasources/MeasurementsDataSourceFactory";
import { MicroclimateContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MeasurementsRepository } from "#ie/repositories";
import { RefreshMeasurementsByIdTaskSchema } from "#ie/schema/RefreshMeasurementsByIdTaskSchema";
import { MeasurementsTransformation } from "#ie/transformations/MeasurementsTransformation";
import { IRefreshMeasurementsByIdTask } from "#sch/Measurements";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";

export class RefreshMeasurementsByIdTask extends AbstractTask<IRefreshMeasurementsByIdTask> {
    public readonly queueName = "refreshMeasurementsById";
    public readonly schema = RefreshMeasurementsByIdTaskSchema;

    private transformation: MeasurementsTransformation;
    private repository: MeasurementsRepository;
    private microclimateRedisChannel: RedisPubSubChannel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.transformation = new MeasurementsTransformation();
        this.repository = new MeasurementsRepository();
        this.microclimateRedisChannel = MicroclimateContainer.resolve<RedisPubSubChannel>(
            ModuleContainerToken.RedisPubSubChannel
        );
    }

    protected async execute(msg: IRefreshMeasurementsByIdTask, msgProperties?: MessageProperties): Promise<void> {
        let errorMessage: string | null = null;

        try {
            const dataSource = MeasurementsDataSourceFactory.getDataSource(msg);
            const data = await dataSource.getAll();
            const transformedData = await this.transformation.transform(data, msg.sensorAddr);
            await this.repository.saveBulk(transformedData);
        } catch (err) {
            errorMessage = err.message;
        }

        if (msgProperties?.headers[ORIGIN_HOSTNAME_HEADER]) {
            await this.microclimateRedisChannel.publishMessage(`${errorMessage ?? "OK"}`, {
                channelSuffix: msgProperties.headers[ORIGIN_HOSTNAME_HEADER],
            });
        }

        if (errorMessage) {
            throw new GeneralError(errorMessage, this.constructor.name);
        }
    }
}
