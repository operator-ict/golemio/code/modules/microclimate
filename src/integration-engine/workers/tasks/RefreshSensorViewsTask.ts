import { MICROCLIMATE_WORKER_NAME } from "#ie/const";
import { RawQueryProvider } from "#ie/helpers/RawQueryProvider";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RefreshSensorViewsTask extends AbstractEmptyTask {
    public readonly queueName = "refreshSensorViews";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes
    private lockTimeout: number;

    constructor(
        @inject(CoreToken.SimpleConfig) config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ContainerToken.RedisConnector) private redisClient: IoRedisConnector,
        @inject(CoreToken.PostgresConnector) private postgresClient: IDatabaseConnector
    ) {
        super(MICROCLIMATE_WORKER_NAME);
        this.lockTimeout = config.getValue<number>(
            "old.datasources.microclimate.sensorMeasurements.mutexLockTimeout",
            9 * 60 * 1000
        ) as number; // default 9 minutes
    }

    protected async execute() {
        const mutex = this.createMutex();

        let lockAcquired = false;
        try {
            lockAcquired = await mutex.tryAcquire();
        } catch (err) {
            throw new GeneralError(`${this.constructor.name}: error while acquiring mutex lock`, this.constructor.name, err);
        }

        if (!lockAcquired) {
            this.logger.info(`${this.constructor.name}: mutex lock was not acquired`);
            return;
        }

        const connection = this.postgresClient.getConnection();
        try {
            // refresh points heights (from imported sensor devices)
            await connection.query(RawQueryProvider.getRefreshPointsHeightsQuery());

            // refresh sensor measurements views
            await connection.query(RawQueryProvider.getRefreshSensorMeasurementsQuery());

            // swap views
            await connection.query(RawQueryProvider.getSwapSensorMeasurementsQuery());
        } catch (err) {
            throw new GeneralError(`${this.constructor.name}: error while refreshing sensor views`, this.constructor.name, err);
        } finally {
            await mutex.release().catch((err) => {
                this.logger.error(`${this.constructor.name}: error while releasing mutex: ${err.message}`);
            });
        }
    }

    private createMutex() {
        return new Mutex(this.redisClient.getConnection(), this.queuePrefix, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            onLockLost: (err) => {
                this.logger.error(err, `${this.constructor.name}: mutex lock was lost`);
            },
        });
    }
}
