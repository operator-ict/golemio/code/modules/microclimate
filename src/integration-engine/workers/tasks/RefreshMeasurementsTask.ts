import { ORIGIN_HOSTNAME_HEADER } from "#ie/const";
import { MicroclimateContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { SensorDevicesRepository } from "#ie/repositories";
import { DateRange, IRefreshMeasurementsByIdTask } from "#sch/Measurements";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import os from "node:os";

export interface ISensorIdObject {
    sensor_id: string;
}

export class RefreshMeasurementsTask extends AbstractEmptyTask {
    public readonly queueName = "refreshMeasurements";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private logger: ILogger;
    private repository: SensorDevicesRepository;
    private microclimateRedisChannel: RedisPubSubChannel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.repository = new SensorDevicesRepository();
        this.logger = MicroclimateContainer.resolve<ILogger>(CoreToken.Logger);
        this.microclimateRedisChannel = MicroclimateContainer.resolve<RedisPubSubChannel>(
            ModuleContainerToken.RedisPubSubChannel
        );
    }

    protected async execute(): Promise<void> {
        const sensorDevicesFromDB: ISensorIdObject[] = await this.repository.find({
            attributes: ["sensor_id"],
            raw: true,
        });

        if (sensorDevicesFromDB.length === 0) {
            this.logger.warn("SensorMeasurements: no sensor devices found");
            return;
        }

        const subscriber = this.microclimateRedisChannel.createSubscriber({
            channelSuffix: os.hostname(),
            maxMessageCount: sensorDevicesFromDB.length,
        });
        await subscriber.subscribe();

        for (const sensorIdObject of sensorDevicesFromDB) {
            const msg: IRefreshMeasurementsByIdTask = {
                sensorAddr: sensorIdObject.sensor_id,
                dateRange: DateRange.TODAY,
            };

            QueueManager.sendMessageToExchange(this.queuePrefix, "refreshMeasurementsById", msg, {
                headers: {
                    [ORIGIN_HOSTNAME_HEADER]: os.hostname(),
                },
            });
        }

        // Listen for messages from workers and wait for all of them to finish
        // before continuing to the next step (refresh sensor views)
        try {
            await subscriber.listen(({ message, messageCount, isFinal }) => {
                this.logger.info(
                    `SensorMeasurements: received message: ${message} (${messageCount}/${sensorDevicesFromDB.length})`
                );

                if (!message.startsWith("OK")) {
                    this.logger.error(`SensorMeasurements: subscription error: ${message}`);
                }

                if (isFinal) {
                    this.logger.info("SensorMeasurements: all messages received");
                }
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }
            throw new GeneralError("Error while listening for messages", this.constructor.name, err);
        } finally {
            await subscriber.unsubscribe();
            QueueManager.sendMessageToExchange(this.queuePrefix, "refreshSensorViews", {});
        }
    }
}
