import { Microclimate } from "#sch";
import { IRefreshMeasurementsByIdTask } from "#sch/Measurements";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MeasurementsDataSourceFactory {
    public static getDataSource(params: IRefreshMeasurementsByIdTask): DataSource {
        const {
            sensorAddr,
            dateRange,
            // For future FROM-TO implementation:
            // dateFrom,
            // dateTo
        } = params;
        return new DataSource(
            Microclimate.datasources.measurementsDatasource.name,
            new HTTPFetchProtocolStrategy({
                headers: { Authorization: "Bearer " + config.datasources.MicroclimateAgdataCityApikey },
                method: "GET",
                url: `${config.datasources.MicroclimateAgdataCity}?sensorAddr=${sensorAddr}&dateRange=${dateRange}`,
            }),
            new JSONDataTypeStrategy({ resultsPath: "data" }),
            new JSONSchemaValidator(
                Microclimate.datasources.measurementsDatasource.name + "Validator",
                Microclimate.datasources.measurementsDatasource.jsonSchema,
                true
            )
        );
    }
}
