import { Microclimate } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class SensorDevicesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            Microclimate.datasources.sensorDevicesDatasource.name,
            new HTTPFetchProtocolStrategy({
                headers: { Authorization: "Bearer " + config.datasources.MicroclimateAgdataCityApikey },
                method: "GET",
                url: config.datasources.MicroclimateAgdataCity + "/devices",
            }),
            new JSONDataTypeStrategy({ resultsPath: "data" }),
            new JSONSchemaValidator(
                Microclimate.datasources.sensorDevicesDatasource.name + "Validator",
                Microclimate.datasources.sensorDevicesDatasource.jsonSchema,
                true
            )
        );
    }
}
