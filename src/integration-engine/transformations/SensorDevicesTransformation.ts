import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IDeviceInput } from "#sch/datasources/SensorDevicesJsonSchema";
import { ISensorDevice, sensorDevices } from "#sch/SensorDevices";

export class SensorDevicesTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = sensorDevices.name;
    }

    public transform = async (data: IDeviceInput[]): Promise<ISensorDevice[]> => {
        const devices: ISensorDevice[] = [];

        for (const item of data) {
            const transformedData = await this.transformElement(item);
            devices.push(transformedData);
        }

        return devices;
    };

    protected transformElement = async (element: IDeviceInput): Promise<ISensorDevice> => {
        const res: ISensorDevice = {
            whole_name: element.name,
            sensor_id: element.address,
        };

        if (element.location.lat && element.location.lng) {
            res.coordinate_lat = element.location.lat;
            res.coordinate_lng = element.location.lng;
        }

        return res;
    };
}
