import { IMeasurementInput } from "#sch/datasources/MeasurementsJsonSchema";
import { IMeasurements, measurements } from "#sch/Measurements";

export class MeasurementsTransformation {
    public name: string;

    constructor() {
        this.name = measurements.name;
    }

    public transform = async (data: IMeasurementInput[], sensor_id: string): Promise<IMeasurements[]> => {
        const measurementsArr: IMeasurements[] = [];

        for (const item of data) {
            const transformedData = await this.transformElement(item, sensor_id);
            measurementsArr.push(transformedData);
        }

        return measurementsArr;
    };

    protected transformElement = async (element: IMeasurementInput, sensor_id: string): Promise<IMeasurements> => {
        const res: IMeasurements = {
            sensor_id,
            measurement_id: element.id,
            measured_at: element.date,
            air_temp: element.data.airTemp1,
            air_hum: element.data.airHum1,
            pressure: element.data.pressure,
            wind_dir: element.data.windDir,
            wind_speed: element.data.windSpeed,
            wind_impact: element.data.windImpact,
            precip: element.data.rainVolume,
            sun_irr: element.data.irr,
            dendro_circ: element.data.dendroCirc,
            dendro_gain: element.data.dendroGain,
            water_pot: element.data.waterPot,
            soil_temp: element.data.temp1,
        };

        return res;
    };
}
