const ModuleContainerToken = {
    RedisPubSubChannel: Symbol(),
    RefreshSensorViewsTask: Symbol(),
};

export { ModuleContainerToken };
