import { MICROCLIMATE_WORKER_NAME } from "#ie/const";
import { RefreshSensorViewsTask } from "#ie/workers/tasks/RefreshSensorViewsTask";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { ModuleContainerToken } from "./ModuleContainerToken";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";

//#region Initialization
// Explicit annotation is needed here for local development with linked core module
const MicroclimateContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Helpers
MicroclimateContainer.registerInstance(ModuleContainerToken.RedisPubSubChannel, new RedisPubSubChannel(MICROCLIMATE_WORKER_NAME));
//#endregion

//#region Tasks
MicroclimateContainer.registerSingleton(ModuleContainerToken.RefreshSensorViewsTask, RefreshSensorViewsTask);
//#endregion

export { MicroclimateContainer };
