import { IsString, IsEnum } from "@golemio/core/dist/shared/class-validator";
import { DateRange, IRefreshMeasurementsByIdTask } from "#sch/Measurements";

export class RefreshMeasurementsByIdTaskSchema implements IRefreshMeasurementsByIdTask {
    @IsString()
    sensorAddr!: string;

    @IsEnum(DateRange)
    dateRange!: DateRange;
    // For future FROM-TO implementation:
    // @IsISO8601()
    // dateFrom!: string;
    // @IsISO8601()
    // dateTo!: string;
}
