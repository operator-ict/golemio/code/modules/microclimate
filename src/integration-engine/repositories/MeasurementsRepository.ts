import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Microclimate } from "#sch";
import { IMeasurements } from "#sch/Measurements";
import { MeasurementsModel } from "#sch/models/MeasurementsModel";

export class MeasurementsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MeasurementsRepository",
            {
                outputSequelizeAttributes: MeasurementsModel.attributeModel,
                pgTableName: Microclimate.definitions.measurements.pgTableName,
                pgSchema: Microclimate.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("MeasurementsRepositoryValidator", MeasurementsModel.jsonSchema)
        );
    }

    public saveBulk = async (data: IMeasurements[]) => {
        if (data && data.length && (await this.validate(data))) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof IMeasurements>((el) => el as keyof IMeasurements);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<MeasurementsModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
