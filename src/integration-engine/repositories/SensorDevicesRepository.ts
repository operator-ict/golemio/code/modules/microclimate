import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Microclimate } from "#sch";
import { ISensorDevice } from "#sch/SensorDevices";
import { SensorDevicesModel } from "#sch/models/SensorDevicesModel";

export class SensorDevicesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "SensorDevicesRepository",
            {
                outputSequelizeAttributes: SensorDevicesModel.attributeModel,
                pgTableName: Microclimate.definitions.sensorDevices.pgTableName,
                pgSchema: Microclimate.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("SensorDevicesRepositoryValidator", SensorDevicesModel.jsonSchema)
        );
    }

    public saveBulk = async (data: ISensorDevice[]) => {
        if (data && data.length && (await this.validate(data))) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof ISensorDevice>((el) => el as keyof ISensorDevice);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<SensorDevicesModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
