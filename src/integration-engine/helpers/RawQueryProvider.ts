import { Microclimate } from "#sch";

export class RawQueryProvider {
    public static getRefreshPointsHeightsQuery() {
        // only very short lock time is expected (around 600 ms at most)
        // hence no need to use concurrent refresh
        return `refresh materialized view ${Microclimate.pgSchema}.v_sensor_points_heights;`;
    }

    public static getRefreshSensorMeasurementsQuery() {
        return `
            drop materialized view if exists ${Microclimate.pgSchema}.v_sensor_measurements_latest_tmp;
            drop materialized view if exists ${Microclimate.pgSchema}.v_sensor_measurements_tmp;

            create materialized view ${Microclimate.pgSchema}.v_sensor_measurements_tmp as
                select * from ${Microclimate.pgSchema}.get_sensor_measurements()
                with no data;
            create index on ${Microclimate.pgSchema}.v_sensor_measurements_tmp (point_id, measure, measured_at desc);
            create index on ${Microclimate.pgSchema}.v_sensor_measurements_tmp (location_id);
            refresh materialized view ${Microclimate.pgSchema}.v_sensor_measurements_tmp;

            create materialized view ${Microclimate.pgSchema}.v_sensor_measurements_latest_tmp as
            select
                point_id,
                measure,
                max(measured_at) as measured_at
            from
                ${Microclimate.pgSchema}.v_sensor_measurements_tmp
            group by
                point_id,
                measure;

            create unique index on ${Microclimate.pgSchema}.v_sensor_measurements_latest_tmp (point_id, measure);`;
    }

    public static getSwapSensorMeasurementsQuery() {
        return `
            drop materialized view ${Microclimate.pgSchema}.v_sensor_measurements_latest;
            drop materialized view ${Microclimate.pgSchema}.v_sensor_measurements;
            alter materialized view ${Microclimate.pgSchema}.v_sensor_measurements_tmp
                rename to v_sensor_measurements;
            alter materialized view ${Microclimate.pgSchema}.v_sensor_measurements_latest_tmp
                rename to v_sensor_measurements_latest;`;
    }
}
