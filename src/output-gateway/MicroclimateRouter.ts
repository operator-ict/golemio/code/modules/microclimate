import { LocationsOutputMapper } from "#og/helpers/LocationsOutputMapper";
import { PointsOutputMapper } from "#og/helpers/PointsOutputMapper";
import { MicroClimateContainer } from "#og/ioc/Di";
import { SensorDevicesImportRepository } from "#og/repositories";
import { SensorMeasurementsViewRepository } from "#og/repositories/SensorMeasurementsViewRepository";
import { SensorPointsViewRepository } from "#og/repositories/SensorPointsViewRepository";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";

class MicroclimateRouter extends BaseRouter {
    public router: Router = Router();
    private sensorDevicesImportRepository: SensorDevicesImportRepository;
    private sensorPointsViewRepository: SensorPointsViewRepository;
    private sensorMeasurementsViewRepository: SensorMeasurementsViewRepository;
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super();
        this.cacheHeaderMiddleware = MicroClimateContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);

        this.sensorDevicesImportRepository = new SensorDevicesImportRepository();
        this.sensorPointsViewRepository = new SensorPointsViewRepository();
        this.sensorMeasurementsViewRepository = new SensorMeasurementsViewRepository();
        this.initRoutes();
    }

    private getLocations = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const data = await this.sensorDevicesImportRepository.GetAllDistinctLocations({
                locationId: req.query.locationId ? Number(req.query.locationId) : undefined,
            });
            res.status(200).send(LocationsOutputMapper.locationsMapper(data));
        } catch (err) {
            next(err);
        }
    };

    private getPoints = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const data = await this.sensorPointsViewRepository.GetAllPoints({
                locationId: req.query.locationId ? Number(req.query.locationId) : undefined,
                pointId: req.query.pointId ? Number(req.query.pointId) : undefined,
            });
            res.status(200).send(PointsOutputMapper.pointsMapper(data));
        } catch (err) {
            next(err);
        }
    };

    private getMeasurements = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const data = await this.sensorMeasurementsViewRepository.GetAllMeasurements({
                locationId: req.query.locationId ? Number(req.query.locationId) : undefined,
                pointId: req.query.pointId ? Number(req.query.pointId) : undefined,
                measure: req.query.measure as string,
                from: req.query.from as string,
                to: req.query.to as string,
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
            });
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    private initRoutes = (): void => {
        this.router.get(
            "/locations",
            [query("locationId").optional().isInt().not().isArray()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.getLocations
        );
        this.router.get(
            "/points",
            [query("locationId").optional().isInt().not().isArray(), query("pointId").optional().isInt().not().isArray()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.getPoints
        );
        this.router.get(
            "/measurements",
            [
                query("locationId").optional().isInt().not().isArray(),
                query("pointId").optional().isInt().not().isArray(),
                query("measure").optional().not().isEmpty({ ignore_whitespace: true }),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("MicroclimateRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.getMeasurements
        );
    };
}

const microclimateRouter: Router = new MicroclimateRouter().router;

export { microclimateRouter };
