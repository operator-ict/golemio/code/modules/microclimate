import { ILocationRecord } from "#og/repositories";

interface IPointOutput {
    point_id: number;
    point_name: string;
}

export interface ILocationOutput {
    location_id: number;
    location: string;
    loc_description: string;
    loc_orientation: string;
    loc_surface: string;
    address: string;
    points: IPointOutput[];
}

export class LocationsOutputMapper {
    public static locationsMapper = (records: ILocationRecord[]): ILocationOutput[] => {
        const locationMap: Record<string, ILocationOutput> = {};
        for (const record of records) {
            if (locationMap[record.location_id]) {
                locationMap[record.location_id].points.push({
                    point_id: record.point_id,
                    point_name: record.point_name,
                });
            } else {
                locationMap[record.location_id] = {
                    location_id: record.location_id,
                    location: record.location,
                    loc_description: record.loc_description,
                    loc_orientation: record.loc_orientation,
                    loc_surface: record.loc_surface,
                    address: record.address,
                    points: [
                        {
                            point_id: record.point_id,
                            point_name: record.point_name,
                        },
                    ],
                };
            }
        }

        return Object.values<ILocationOutput>(locationMap);
    };
}
