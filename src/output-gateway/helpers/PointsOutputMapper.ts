import { ISensorPointsView } from "#sch/SensorPointsView";

interface ISensorPointsViewMeasurementOutput {
    measure: string;
    measure_cz: string;
    unit: string;
}

export interface ISensorPointsViewOutput {
    point_id: number;
    location_id: number;
    point_name: string;
    location: string;
    loc_description: string;
    loc_orientation: string;
    loc_surface: string;
    lat: number;
    lng: number;
    x_jtsk: number | null;
    y_jtsk: number | null;
    elevation_m: number | null;
    measures: ISensorPointsViewMeasurementOutput[];
    sensor_position: string;
    sensor_position_detail: string;
}

export class PointsOutputMapper {
    public static pointsMapper = (records: ISensorPointsView[]): ISensorPointsViewOutput[] => {
        const pointsMap: Record<string, ISensorPointsViewOutput> = {};
        for (const record of records) {
            if (pointsMap[record.point_id]) {
                pointsMap[record.point_id].measures.push({
                    measure: record.measure,
                    measure_cz: record.measure_cz,
                    unit: record.unit,
                });
            } else {
                pointsMap[record.point_id] = {
                    point_id: record.point_id,
                    location_id: record.location_id,
                    point_name: record.point_name,
                    location: record.location,
                    loc_description: record.loc_description,
                    loc_orientation: record.loc_orientation,
                    loc_surface: record.loc_surface,
                    lat: record.lat,
                    lng: record.lng,
                    x_jtsk: record.x_jtsk,
                    y_jtsk: record.y_jtsk,
                    elevation_m: record.elevation_m,
                    measures: [
                        {
                            measure: record.measure,
                            measure_cz: record.measure_cz,
                            unit: record.unit,
                        },
                    ],
                    sensor_position: record.sensor_position,
                    sensor_position_detail: record.sensor_position_detail,
                };
            }
        }

        return Object.values<ISensorPointsViewOutput>(pointsMap);
    };
}
