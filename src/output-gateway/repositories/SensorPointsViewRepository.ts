import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { Microclimate } from "#sch";
import { SensorPointsViewModel } from "#sch/models/SensorPointsViewModel";
import { ISensorPointsView } from "#sch/SensorPointsView";
import { Op, WhereOptions } from "@golemio/core/dist/shared/sequelize";

export class SensorPointsViewRepository extends SequelizeModel {
    public constructor() {
        super(
            Microclimate.definitions.sensorPointsView.name,
            Microclimate.definitions.sensorPointsView.pgTableName,
            SensorPointsViewModel.attributeModel,
            {
                schema: Microclimate.pgSchema,
            }
        );
        this.sequelizeModel.removeAttribute("id");
    }

    public GetAllPoints = async (
        options: {
            locationId?: number;
            pointId?: number;
        } = {}
    ): Promise<ISensorPointsView[]> => {
        const whereAttributes: WhereOptions = {
            [Op.and]: [
                ...(options.locationId ? [{ location_id: { [Op.eq]: options.locationId } }] : []),
                ...(options.pointId ? [{ point_id: { [Op.eq]: options.pointId } }] : []),
            ],
        };

        return this.sequelizeModel.findAll({
            where: whereAttributes,
        });
    };

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}
