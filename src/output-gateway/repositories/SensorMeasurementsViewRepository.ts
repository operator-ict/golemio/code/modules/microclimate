import { Microclimate } from "#sch";
import { ISensorMeasurementsView } from "#sch/SensorMeasurementsView";
import { SensorMeasurementsLatestViewModel } from "#sch/models/SensorMeasurementsLatestViewModel";
import { SensorMeasurementsViewModel } from "#sch/models/SensorMeasurementsViewModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { ModelStatic, Op, Sequelize, WhereOptions } from "@golemio/core/dist/shared/sequelize";

export class SensorMeasurementsViewRepository extends SequelizeModel {
    private measurementsLatestViewModel: ModelStatic<SensorMeasurementsLatestViewModel>;

    public constructor() {
        super(
            Microclimate.definitions.sensorMeasurementsView.name,
            Microclimate.definitions.sensorMeasurementsView.pgTableName,
            SensorMeasurementsViewModel.attributeModel,
            {
                schema: Microclimate.pgSchema,
                timestamps: false,
            }
        );
        this.sequelizeModel.removeAttribute("id");

        // Associations
        this.measurementsLatestViewModel = this.sequelizeModel["sequelize"]!.define(
            Microclimate.definitions.sensorMeasurementsLatestView.pgTableName,
            SensorMeasurementsLatestViewModel.attributeModel,
            {
                schema: Microclimate.pgSchema,
                timestamps: false,
            }
        );

        this.sequelizeModel.hasOne(this.measurementsLatestViewModel, {
            foreignKey: "point_id",
            sourceKey: "point_id",
            as: "latest",
        });
    }

    public GetAllMeasurements = async (
        options: {
            locationId?: number;
            pointId?: number;
            measure?: string;
            from?: string;
            to?: string;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<ISensorMeasurementsView[]> => {
        const whereAttributes: WhereOptions = {
            [Op.and]: [
                ...(options.locationId ? [{ location_id: { [Op.eq]: options.locationId } }] : []),
                ...(options.pointId ? [{ point_id: { [Op.eq]: options.pointId } }] : []),
                ...(options.measure ? [{ measure: { [Op.eq]: options.measure } }] : []),
                ...(options.from ? [{ measured_at: { [Op.gte]: options.from } }] : []),
                ...(options.to ? [{ measured_at: { [Op.lte]: options.to } }] : []),
            ],
        };

        const joinAttributes: WhereOptions = {
            [Op.and]: [Sequelize.literal("latest.measure = v_sensor_measurements.measure")],
            // only if from and to are not set
            // then we only want to get the latest measurement for each point and measure type
            ...(!options.from && !options.to
                ? [Sequelize.literal("latest.measured_at = v_sensor_measurements.measured_at")]
                : []),
        };

        return this.sequelizeModel.findAll({
            attributes: {
                exclude: ["sensor_id"],
            },
            include: [
                {
                    model: this.measurementsLatestViewModel,
                    as: "latest",
                    attributes: [],
                    where: joinAttributes,
                },
            ],
            where: whereAttributes,
            limit: options.limit,
            offset: options.offset ?? 0,
            order: [
                ["point_id", "asc"],
                ["measure", "asc"],
                ["measured_at", "desc"],
            ],
        });
    };

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}
