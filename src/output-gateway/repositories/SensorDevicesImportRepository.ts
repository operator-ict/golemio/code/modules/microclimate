import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { Microclimate } from "#sch";
import { SensorDevicesImportModel } from "#sch/models/SensorDevicesImportModel";

export interface ILocationRecord {
    location_id: number;
    location: string;
    loc_description: string;
    loc_orientation: string;
    loc_surface: string;
    address: string;
    point_id: number;
    point_name: string;
}

export class SensorDevicesImportRepository extends SequelizeModel {
    public constructor() {
        super(
            Microclimate.definitions.sensorDevicesImport.name,
            Microclimate.definitions.sensorDevicesImport.pgTableName,
            SensorDevicesImportModel.attributeModel,
            {
                schema: Microclimate.pgSchema,
            }
        );
    }

    public GetAllDistinctLocations = async (
        options: {
            locationId?: number;
        } = {}
    ): Promise<ILocationRecord[]> => {
        let whereCondition = "";
        if (options.locationId) {
            whereCondition = `WHERE location_id = ${options.locationId}`;
        }

        return this.sequelizeModel.sequelize!.query<ILocationRecord>(
            `
            SELECT DISTINCT
                location_id,
                point_id,
                location,
                loc_description,
                loc_orientation,
                loc_surface,
                point_name,
                address
            FROM ${Microclimate.pgSchema}.sensor_devices_import
            ${whereCondition}
            ORDER BY location_id ASC, point_id ASC
            ;
        `,
            { type: Sequelize.QueryTypes.SELECT }
        );
    };

    GetAll(): never {
        throw new Error("Not implemented");
    }

    GetOne(): never {
        throw new Error("Not implemented");
    }
}
