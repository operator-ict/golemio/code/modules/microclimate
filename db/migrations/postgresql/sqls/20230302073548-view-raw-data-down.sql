DROP VIEW analytic.v_microclimate_sensor_devices;

CREATE OR REPLACE VIEW analytic.v_microclimate_sensor_devices
AS SELECT sdi.sensor_id,
    sdi.location_id,
    sdi.point_id,
    sdi.location,
    sdi.loc_description,
    sdi.loc_orientation,
    sdi.loc_surface,
    sdi.point_name,
    sdi.address,
    sdi.sensor_position,
    sdi.sensor_position_detail,
    sdi.lat,
    sdi.lng,
    ('https://storage.golemio.cz/oict-mikroklima/'::text || sdi.sensor_id::text) || '.jpg'::text AS url_foto
   FROM microclimate.sensor_devices_import sdi;

DROP VIEW analytic.v_microclimate_raw_data;
