CREATE TABLE IF NOT EXISTS sensor_devices (
    "whole_name" text NOT NULL,
    "sensor_id" varchar(50) NOT NULL,
    "coordinate_lat" float,
    "coordinate_lng" float,

    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT sensor_devices_pkey PRIMARY KEY ("sensor_id")
);

CREATE TABLE IF NOT EXISTS measurements (
    "sensor_id" varchar(50) NOT NULL,
    "measurement_id" varchar(50) NOT NULL,
    "measured_at" timestamptz  NOT NULL,
    "air_temp" float,
    "air_hum" float,
    "pressure" int,
    "wind_dir" varchar(150),
    "wind_speed" float,
    "wind_impact" float,
    "precip" float,
    "sun_irr" int,
    "dendro_circ" float,
    "dendro_gain" float,
    "water_pot" float,
    "soil_temp" float,

    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT measurements_pkey PRIMARY KEY ("measurement_id")
);




