create materialized view v_sensor_points_heights as
select s.sensor_id,
s.point_id,
s.location_id,
s.data_relevance,
s.data_until,
unpivot.measure,
unpivot.height_cm,
    CASE
        WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
        WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
        WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
        WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
        WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
        WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
        WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
        WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
        WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
        WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
        WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
        WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
        ELSE NULL::text
    END AS unit
from
    sensor_devices_import s,
    LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
where unpivot.height_cm IS NOT NULL;

create index v_sensor_points_heights_idx ON v_sensor_points_heights (sensor_id, measure, data_relevance, data_until);


create function get_sensor_measurements()
    returns table(
        sensor_id varchar(50),
        measured_at timestamptz,
        point_id int,
        location_id int,
        measure text,
        "value" float8,
        unit text
    )
    language sql
    set search_path from current
AS $function$
with Measures as (
	select unnest(array[
	'air_temp', 'air_hum', 'pressure',
    'wind_dir', 'wind_impact', 'wind_speed',
    'precip', 'sun_irr', 'soil_temp',
    'water_pot', 'dendro_circ', 'dendro_gain'
	]) AS measure
), MeasurementsData as (
    select m.sensor_id,
    	sdi.point_id,
    	sdi.location_id,
    	sdi.unit,
        m.measured_at,
        Measures.measure || height_cm as measure,
        CASE
            WHEN Measures.measure = 'wind_dir' THEN (
                CASE
                    WHEN m.wind_dir::text = 'N'::text THEN 0
                        WHEN m.wind_dir::text = 'NE'::text THEN 45
                        WHEN m.wind_dir::text = 'E'::text THEN 90
                        WHEN m.wind_dir::text = 'SE'::text THEN 135
                        WHEN m.wind_dir::text = 'S'::text THEN 180
                        WHEN m.wind_dir::text = 'SW'::text THEN 225
                        WHEN m.wind_dir::text = 'W'::text THEN 270
                        WHEN m.wind_dir::text = 'NW'::text THEN 315
                    ELSE NULL::integer
                END
            )
            when Measures.measure = 'air_temp' then m.air_temp
            when Measures.measure = 'air_hum' then m.air_hum
            when Measures.measure = 'pressure' then m.pressure
            when Measures.measure = 'wind_impact' then m.wind_impact
            when Measures.measure = 'wind_speed' then m.wind_speed
            when Measures.measure = 'precip' then m.precip
            when Measures.measure = 'sun_irr' then m.sun_irr
            when Measures.measure = 'soil_temp' then m.soil_temp
            when Measures.measure = 'water_pot' then m.water_pot
            when Measures.measure = 'dendro_circ' then m.dendro_circ
            when Measures.measure = 'dendro_gain' then m.dendro_gain
            ELSE sdi.height_cm
        END AS value
    from
        measurements m
    cross join Measures
    join v_sensor_points_heights sdi
        ON sdi.sensor_id = m.sensor_id and sdi.measure = Measures.measure
    where m.measured_at::date >= sdi.data_relevance AND m.measured_at::date <= coalesce(sdi.data_until, current_date)
)
select
    sensor_id,
    measured_at,
    point_id,
    location_id,
    measure,
    "value",
    unit
from
    MeasurementsData
where "value" is not null
$function$;


create materialized view v_sensor_measurements_tmp as
select * from get_sensor_measurements()
with no data;


-- create indexes
create index on v_sensor_measurements_tmp (point_id, measure, measured_at desc);
create index on v_sensor_measurements_tmp (location_id);


-- populate the materialized view
refresh materialized view v_sensor_measurements_tmp;

create materialized view v_sensor_measurements_latest_tmp as
select
    point_id,
    measure,
    max(measured_at) as measured_at
from
    v_sensor_measurements_tmp
group by
    point_id,
    measure;

create unique index on v_sensor_measurements_latest_tmp (point_id, measure);

-- replace current view with the new materialized view
alter view v_sensor_measurements rename to v_sensor_measurements_old;
alter materialized view v_sensor_measurements_tmp rename to v_sensor_measurements;
alter materialized view v_sensor_measurements_latest_tmp rename to v_sensor_measurements_latest;
drop view v_sensor_measurements_old;
