DROP VIEW IF EXISTS v_sensor_points;
DROP VIEW IF EXISTS v_sensor_measurements;

DROP INDEX measured_at_idx;
DROP INDEX sensor_id_idx;
