CREATE OR REPLACE VIEW v_sensor_points
AS SELECT DISTINCT s.point_id,
    s.point_name,
    s.location_id,
    s.location,
    s.loc_description,
    s.loc_orientation,
    s.loc_surface,
    s.lat,
    s.lng,
    NULL::text AS x_jtsk,
    NULL::text AS y_jtsk,
    NULL::text AS elevation_m,
    s.sensor_position,
    s.sensor_position_detail,
    concat(unpivot.measure, unpivot.height_cm) AS measure,
    concat(
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN 'Teplota vzduchu'::text
            WHEN unpivot.measure = 'air_hum'::text THEN 'Vlhkost vzduchu'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Atmosférický tlak'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN 'Směr větru'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'Náraz větru'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'Rychlost větru'::text
            WHEN unpivot.measure = 'precip'::text THEN 'Úhrn srážek'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'Sluneční záření'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN 'Teplota půdy'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'Vodní potenciál půdy'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'Obvod stromu'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'Přírůstek obvodu stromu'::text
            ELSE NULL::text
        END, ', ', unpivot.height_cm, ' cm'
    ) AS measure_cz,
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
            ELSE NULL::text
        END AS unit
   FROM microclimate.sensor_devices_import s,
    LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
  WHERE unpivot.height_cm IS NOT NULL
  ORDER BY s.point_id;
