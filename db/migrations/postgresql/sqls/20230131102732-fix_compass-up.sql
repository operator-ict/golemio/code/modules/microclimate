-- analytic.v_microclimate_measurements_daily source

CREATE OR REPLACE VIEW analytic.v_microclimate_measurements_daily
AS WITH calendar AS (
         SELECT calendar_1.date,
            sensors_1.sensor_id
           FROM ( SELECT generate_series('2022-08-02 00:00:00+02'::timestamp with time zone, CURRENT_DATE::timestamp with time zone, '1 day'::interval)::date AS date) calendar_1
             CROSS JOIN ( SELECT DISTINCT sensor_devices_import.sensor_id
                   FROM microclimate.sensor_devices_import) sensors_1
        ), first_measurement_per_hour AS (
         SELECT DISTINCT ON (m.sensor_id, (timezone('CET'::text, m.measured_at)::date), (date_part('hour'::text, timezone('CET'::text, m.measured_at)))) m.sensor_id,
            timezone('CET'::text, m.measured_at) AS measured_at,
            timezone('CET'::text, m.measured_at)::date AS date,
            date_part('hour'::text, timezone('CET'::text, m.measured_at)) AS hour,
            m.air_temp,
            m.air_hum,
            m.pressure,
            m.wind_speed,
            m.water_pot,
            m.soil_temp
           FROM microclimate.measurements m
          WHERE date_part('hour'::text, timezone('CET'::text, m.measured_at)) = ANY (ARRAY[7::double precision, 14::double precision, 21::double precision])
          ORDER BY m.sensor_id, (timezone('CET'::text, m.measured_at)::date), (date_part('hour'::text, timezone('CET'::text, m.measured_at))), m.measured_at
        ), seven AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 7::double precision
        ), two AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 14::double precision
        ), nine AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 21::double precision
        ), wind AS (
         SELECT DISTINCT ON (m.sensor_id, (timezone('CET'::text, m.measured_at)::date)) m.sensor_id,
            timezone('CET'::text, m.measured_at)::date AS date,
            m.wind_speed AS max_wind_speed,
                CASE
                    WHEN m.wind_dir = 'N' THEN 0
                    WHEN m.wind_dir = 'NE' THEN 45
                    WHEN m.wind_dir = 'E' THEN 90
                    WHEN m.wind_dir = 'SE' THEN 135
                    WHEN m.wind_dir = 'S' THEN 180
                    WHEN m.wind_dir = 'SW' THEN 225
                    WHEN m.wind_dir = 'W' THEN 270
                    WHEN m.wind_dir = 'NW' THEN 315
                    ELSE NULL END AS wind_dir_at_max_speed
           FROM microclimate.measurements m
          ORDER BY m.sensor_id, (timezone('CET'::text, m.measured_at)::date), m.wind_speed DESC
        ), min_max_daily_temp AS (
         SELECT m.sensor_id,
            timezone('-04'::text, m.measured_at)::date AS date,
            max(m.air_temp) AS max_temperature,
            min(m.air_temp) AS min_temperature
           FROM microclimate.measurements m
          GROUP BY m.sensor_id, (timezone('-04'::text, m.measured_at)::date)
          ORDER BY m.sensor_id, (timezone('-04'::text, m.measured_at)::date)
        ), precip AS (
         SELECT m.sensor_id,
            timezone('+06'::text, m.measured_at)::date AS date,
            sum(m.precip) AS sum_precip
           FROM microclimate.measurements m
          GROUP BY m.sensor_id, (timezone('+06'::text, m.measured_at)::date)
          ORDER BY m.sensor_id, (timezone('+06'::text, m.measured_at)::date)
        ), calc AS (
         SELECT sensors.address,
            sensors.sensor_position_detail,
            sensors.point_id,
            sensors.air_temp,
            sensors.air_hum,
            sensors.pressure,
            sensors.precip,
            sensors.wind_dir,
            sensors.wind_impact,
            sensors.wind_speed,
            sensors.sun_irr,
            sensors.soil_temp,
            sensors.water_pot,
            sensors.dendro_circ,
            sensors.dendro_gain,
            calendar.date,
            calendar.sensor_id,
            (seven.air_temp + two.air_temp + 2::double precision * nine.air_temp) / 4::double precision AS avg_temperature,
            (seven.air_hum + two.air_hum + nine.air_hum) / 3::double precision AS avg_air_hum,
            (seven.pressure + two.pressure + nine.pressure) / 3 / 100 AS avg_pressure,
            (seven.wind_speed + two.wind_speed + nine.wind_speed) / 3::double precision AS avg_wind_speed,
            (seven.water_pot + two.water_pot + nine.water_pot) / 3::double precision AS avg_water_pot,
            (seven.soil_temp + two.soil_temp + nine.soil_temp) / 3::double precision AS avg_soil_temp,
            wind.max_wind_speed,
            wind.wind_dir_at_max_speed,
            min_max_daily_temp.max_temperature,
            min_max_daily_temp.min_temperature,
            precip.sum_precip
           FROM calendar
             LEFT JOIN seven ON calendar.sensor_id::text = seven.sensor_id::text AND calendar.date = seven.date
             LEFT JOIN two ON calendar.sensor_id::text = two.sensor_id::text AND calendar.date = two.date
             LEFT JOIN nine ON calendar.sensor_id::text = nine.sensor_id::text AND calendar.date = nine.date
             LEFT JOIN wind ON calendar.sensor_id::text = wind.sensor_id::text AND calendar.date = wind.date
             LEFT JOIN min_max_daily_temp ON calendar.sensor_id::text = min_max_daily_temp.sensor_id::text AND calendar.date = min_max_daily_temp.date
             LEFT JOIN precip ON calendar.sensor_id::text = precip.sensor_id::text AND calendar.date = precip.date
             LEFT JOIN microclimate.sensor_devices_import sensors ON calendar.sensor_id::text = sensors.sensor_id::text
        )
 SELECT calc.address,
    calc.sensor_position_detail,
    calc.date,
    calc.sensor_id,
    calc.point_id,
    unpivot.metrika,
    unpivot.value
   FROM calc,
    LATERAL ( VALUES ('avg_temperature'::text,calc.avg_temperature), ('avg_air_hum'::text,calc.avg_air_hum), ('avg_pressure'::text,calc.avg_pressure), ('avg_wind_speed'::text,calc.avg_wind_speed), ('avg_water_pot'::text,calc.avg_water_pot), ('avg_soil_temp'::text,calc.avg_soil_temp), ('max_wind_speed'::text,calc.max_wind_speed), ('max_temperature'::text,calc.max_temperature), ('min_temperature'::text,calc.min_temperature), ('sum_precip'::text,calc.sum_precip), ('wind_dir_at_max_speed'::text,calc.wind_dir_at_max_speed)) unpivot(metrika, value)
  WHERE unpivot.value IS NOT NULL;
