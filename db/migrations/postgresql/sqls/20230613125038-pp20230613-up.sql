ALTER TABLE sensor_devices_import ALTER COLUMN data_relevance TYPE date USING data_relevance::date;
ALTER TABLE sensor_devices_import ALTER COLUMN data_until TYPE date USING data_until::date;
