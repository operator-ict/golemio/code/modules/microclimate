CREATE TABLE sensor_devices_import (
	location_id int4 NULL,
	point_id int4 NULL,
	"location" varchar(50) NULL,
	loc_description varchar(50) NULL,
	loc_orientation varchar(50) NULL,
	loc_surface varchar(50) NULL,
	point_name varchar(50) NULL,
	point_whole_name varchar(50) NULL,
	address varchar(50) NULL,
	sensor_position varchar(50) NULL,
	sensor_position_detail varchar(50) NULL,
	sensor_id varchar(50) primary key,
	data_relevance varchar(50) NULL,
	lat float4 NULL,
	lng float4 NULL,
	air_temp int4 NULL,
	air_hum int4 NULL,
	pressure int4 NULL,
	wind_dir int4 NULL,
	wind_impact int4 NULL,
	wind_speed int4 NULL,
	precip int4 NULL,
	sun_irr int4 NULL,
	soil_temp int4 NULL,
	water_pot int4 NULL,
	dendro_circ int4 NULL,
	dendro_gain int4 NULL
);
