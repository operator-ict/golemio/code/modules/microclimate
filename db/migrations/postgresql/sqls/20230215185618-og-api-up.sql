/**
    v_sensor_points view for output api
**/
CREATE OR REPLACE VIEW v_sensor_points AS
SELECT DISTINCT
	s.point_id,
	s.point_name,
	s.location_id,
	s.location,
	s.loc_description,
	s.loc_orientation,
	s.loc_surface,
	s.lat,
	s.lng,
	null AS x_jtsk, -- bude doplneno na s.x_jtsk
	null AS y_jtsk, -- bude doplneno
	null AS elevation_m, -- bude doplneno
	s.sensor_position,
	s.sensor_position_detail,
	CONCAT(unpivot.measure, unpivot.height_cm) AS measure,
    CASE
        WHEN unpivot.measure = 'air_temp' THEN 'Teplota vzduchu'
        WHEN unpivot.measure = 'air_hum' THEN 'Vlhkost vzduchu'
        WHEN unpivot.measure = 'pressure' THEN 'Atmosférický tlak'
        WHEN unpivot.measure = 'wind_dir' THEN 'Směr větru'
        WHEN unpivot.measure = 'wind_impact' THEN 'Náraz větru'
        WHEN unpivot.measure = 'wind_speed' THEN 'Rychlost větru'
        WHEN unpivot.measure = 'precip' THEN 'Úhrn srážek'
        WHEN unpivot.measure = 'sun_irr' THEN 'Sluneční záření'
        WHEN unpivot.measure = 'soil_temp' THEN 'Teplota půdy'
        WHEN unpivot.measure = 'water_pot' THEN 'Vodní potenciál půdy'
        WHEN unpivot.measure = 'dendro_circ' THEN 'Obvod stromu'
        WHEN unpivot.measure = 'dendro_gain' THEN 'Přírůstek obvodu stromu'
        ELSE NULL
    END AS measure_cz,
    CASE
        WHEN unpivot.measure = 'air_temp' THEN '°C'
        WHEN unpivot.measure = 'air_hum' THEN '%'
        WHEN unpivot.measure = 'pressure' THEN 'Pa'
        WHEN unpivot.measure = 'wind_dir' THEN '°'
        WHEN unpivot.measure = 'wind_impact' THEN 'km/h'
        WHEN unpivot.measure = 'wind_speed' THEN 'km/h'
        WHEN unpivot.measure = 'precip' THEN 'mm'
        WHEN unpivot.measure = 'sun_irr' THEN 'lux'
        WHEN unpivot.measure = 'soil_temp' THEN '°C'
        WHEN unpivot.measure = 'water_pot' THEN 'kPa'
        WHEN unpivot.measure = 'dendro_circ' THEN 'mm'
        WHEN unpivot.measure = 'dendro_gain' THEN 'µm'
        ELSE NULL
    END AS unit
FROM
    sensor_devices_import s,
	LATERAL (
        VALUES ('air_temp',s.air_temp),
		('air_hum',s.air_hum),
		('pressure',s.pressure),
		('wind_dir',s.wind_dir),
		('wind_impact',s.wind_impact),
		('wind_speed',s.wind_speed),
		('precip',s.precip),
		('sun_irr',s.sun_irr),
		('soil_temp',s.soil_temp),
		('water_pot',s.water_pot),
		('dendro_circ',s.dendro_circ),
		('dendro_gain',s.dendro_gain)
    )
    unpivot(measure, height_cm)
WHERE height_cm is not null
ORDER BY point_id ASC;


/**
    v_sensor_measurements view for output api
**/
CREATE OR REPLACE VIEW v_sensor_measurements AS
WITH points_heights AS (
    SELECT
        s.sensor_id,
        s.point_id,
        s.location_id,
        unpivot.measure,
        unpivot.height_cm,
        CASE
            WHEN unpivot.measure = 'air_temp' THEN '°C'
            WHEN unpivot.measure = 'air_hum' THEN '%'
            WHEN unpivot.measure = 'pressure' THEN 'Pa'
            WHEN unpivot.measure = 'wind_dir' THEN '°'
            WHEN unpivot.measure = 'wind_impact' THEN 'km/h'
            WHEN unpivot.measure = 'wind_speed' THEN 'km/h'
            WHEN unpivot.measure = 'precip' THEN 'mm'
            WHEN unpivot.measure = 'sun_irr' THEN 'lux'
            WHEN unpivot.measure = 'soil_temp' THEN '°C'
            WHEN unpivot.measure = 'water_pot' THEN 'kPa'
            WHEN unpivot.measure = 'dendro_circ' THEN 'mm'
            WHEN unpivot.measure = 'dendro_gain' THEN 'µm'
            ELSE NULL
        END AS unit
    FROM
        sensor_devices_import s,
        LATERAL (
            VALUES ('air_temp',s.air_temp),
            ('air_hum',s.air_hum),
            ('pressure',s.pressure),
            ('wind_dir',s.wind_dir),
            ('wind_impact',s.wind_impact),
            ('wind_speed',s.wind_speed),
            ('precip',s.precip),
            ('sun_irr',s.sun_irr),
            ('soil_temp',s.soil_temp),
            ('water_pot',s.water_pot),
            ('dendro_circ',s.dendro_circ),
            ('dendro_gain',s.dendro_gain)
        )
        unpivot(measure, height_cm)
    WHERE height_cm IS NOT NULL
),
measurements AS (
    SELECT
        m.sensor_id,
        m.measured_at,
        unpivot.measure,
        unpivot.value
    FROM
        measurements m,
        LATERAL (
            VALUES
            ('air_temp',m.air_temp),
            ('air_hum',m.air_hum),
            ('pressure',m.pressure),
            ('wind_dir', CASE
                WHEN m.wind_dir = 'N' THEN 0
                WHEN m.wind_dir = 'NE' THEN 45
                WHEN m.wind_dir = 'E' THEN 90
                WHEN m.wind_dir = 'SE' THEN 135
                WHEN m.wind_dir = 'S' THEN 180
                WHEN m.wind_dir = 'SW' THEN 225
                WHEN m.wind_dir = 'W' THEN 270
                WHEN m.wind_dir = 'NW' THEN 315
                ELSE NULL END),
            ('wind_impact',m.wind_impact),
            ('wind_speed',m.wind_speed),
            ('precip',m.precip),
            ('sun_irr',m.sun_irr),
            ('soil_temp',m.soil_temp),
            ('water_pot',m.water_pot),
            ('dendro_circ',m.dendro_circ),
            ('dendro_gain',m.dendro_gain)
        )
        unpivot(measure, value)
    WHERE value IS NOT NULL
)
SELECT
    q.*,
	row_number() OVER (PARTITION BY point_id, measure ORDER BY point_id ASC, measure ASC, measured_at DESC) rn
FROM (
	SELECT
		measurements.sensor_id,
		measurements.measured_at,
		points_heights.point_id,
		points_heights.location_id,
		CONCAT(measurements.measure, points_heights.height_cm) AS measure,
		measurements.value,
		points_heights.unit
	FROM measurements
	JOIN points_heights
		ON points_heights.sensor_id = measurements.sensor_id
		AND points_heights.measure = measurements.measure
) as q
ORDER BY point_id ASC, measure ASC, measured_at DESC;

CREATE INDEX measured_at_idx ON measurements (measured_at DESC);
CREATE INDEX sensor_id_idx ON measurements (sensor_id);
