CREATE OR REPLACE VIEW v_sensor_measurements
AS 
WITH points_heights AS (
         SELECT s.sensor_id,
            s.point_id,
            s.location_id,
            unpivot.measure,
            unpivot.height_cm,
                CASE
                    WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
                    WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
                    WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
                    WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
                    WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
                    WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
                    ELSE NULL::text
                END AS unit
           FROM sensor_devices_import s,
            LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
          WHERE unpivot.height_cm IS NOT NULL
        ), measurements AS (
         SELECT m.sensor_id,
            m.measured_at,
            unpivot.measure,
            unpivot.value
           FROM measurements m
           join sensor_devices_import sdi
           on sdi.sensor_id = m.sensor_id
           and m.measured_at::date >= sdi.data_relevance::date
           and m.measured_at::date <= coalesce(sdi.data_until::date,current_date)
           ,
            LATERAL ( VALUES ('air_temp'::text,m.air_temp), ('air_hum'::text,m.air_hum), ('pressure'::text,m.pressure), ('wind_dir'::text,
                        CASE
                            WHEN m.wind_dir::text = 'N'::text THEN 0
                            WHEN m.wind_dir::text = 'NE'::text THEN 45
                            WHEN m.wind_dir::text = 'E'::text THEN 90
                            WHEN m.wind_dir::text = 'SE'::text THEN 135
                            WHEN m.wind_dir::text = 'S'::text THEN 180
                            WHEN m.wind_dir::text = 'SW'::text THEN 225
                            WHEN m.wind_dir::text = 'W'::text THEN 270
                            WHEN m.wind_dir::text = 'NW'::text THEN 315
                            ELSE NULL::integer
                        END), ('wind_impact'::text,m.wind_impact), ('wind_speed'::text,m.wind_speed), ('precip'::text,m.precip), ('sun_irr'::text,m.sun_irr), ('soil_temp'::text,m.soil_temp), ('water_pot'::text,m.water_pot), ('dendro_circ'::text,m.dendro_circ), ('dendro_gain'::text,m.dendro_gain)) unpivot(measure, value)
          WHERE unpivot.value IS NOT NULL
        )
 SELECT q.sensor_id,
    q.measured_at,
    q.point_id,
    q.location_id,
    q.measure,
    q.value,
    q.unit,
    row_number() OVER (PARTITION BY q.point_id, q.measure ORDER BY q.point_id, q.measure, q.measured_at DESC) AS rn
   FROM ( SELECT measurements.sensor_id,
            measurements.measured_at,
            points_heights.point_id,
            points_heights.location_id,
            concat(measurements.measure, points_heights.height_cm) AS measure,
            measurements.value,
            points_heights.unit
           FROM measurements
             JOIN points_heights ON points_heights.sensor_id::text = measurements.sensor_id::text AND points_heights.measure = measurements.measure) q
  ORDER BY q.point_id, q.measure, q.measured_at DESC;
