DROP VIEW microclimate.v_sensor_points;

DROP VIEW microclimate.v_sensor_measurements;

DROP TABLE microclimate.sensor_devices_import;

CREATE TABLE microclimate.sensor_devices_import (
	location_id int4 NULL,
	point_id int4 NULL,
	"location" varchar(50) NULL,
	loc_description varchar(50) NULL,
	loc_orientation varchar(50) NULL,
	loc_surface varchar(50) NULL,
	point_name varchar(50) NULL,
	point_whole_name varchar(50) NULL,
	address varchar(50) NULL,
	sensor_position varchar(50) NULL,
	sensor_position_detail varchar(50) NULL,
	sensor_id varchar(50) NOT NULL,
	data_relevance varchar(50) NULL,
	lat float4 NULL,
	lng float4 NULL,
	air_temp int4 NULL,
	air_hum int4 NULL,
	pressure int4 NULL,
	wind_dir int4 NULL,
	wind_impact int4 NULL,
	wind_speed int4 NULL,
	precip int4 NULL,
	sun_irr int4 NULL,
	soil_temp int4 NULL,
	water_pot int4 NULL,
	dendro_circ int4 NULL,
	dendro_gain int4 NULL,
	CONSTRAINT sensor_devices_import_pkey PRIMARY KEY (sensor_id)
);

CREATE OR REPLACE VIEW microclimate.v_sensor_points
AS SELECT DISTINCT s.point_id,
    s.point_name,
    s.location_id,
    s.location,
    s.loc_description,
    s.loc_orientation,
    s.loc_surface,
    s.lat,
    s.lng,
    NULL::text AS x_jtsk,
    NULL::text AS y_jtsk,
    NULL::text AS elevation_m,
    s.sensor_position,
    s.sensor_position_detail,
    concat(unpivot.measure, unpivot.height_cm) AS measure,
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN 'Teplota vzduchu'::text
            WHEN unpivot.measure = 'air_hum'::text THEN 'Vlhkost vzduchu'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Atmosférický tlak'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN 'Směr větru'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'Náraz větru'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'Rychlost větru'::text
            WHEN unpivot.measure = 'precip'::text THEN 'Úhrn srážek'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'Sluneční záření'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN 'Teplota půdy'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'Vodní potenciál půdy'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'Obvod stromu'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'Přírůstek obvodu stromu'::text
            ELSE NULL::text
        END AS measure_cz,
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
            ELSE NULL::text
        END AS unit
   FROM microclimate.sensor_devices_import s,
    LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
  WHERE unpivot.height_cm IS NOT NULL
  ORDER BY s.point_id;

CREATE OR REPLACE VIEW microclimate.v_sensor_measurements
AS WITH points_heights AS (
         SELECT s.sensor_id,
            s.point_id,
            s.location_id,
            unpivot.measure,
            unpivot.height_cm,
                CASE
                    WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
                    WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
                    WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
                    WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
                    WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
                    WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
                    ELSE NULL::text
                END AS unit
           FROM microclimate.sensor_devices_import s,
            LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
          WHERE unpivot.height_cm IS NOT NULL
        ), measurements AS (
         SELECT m.sensor_id,
            m.measured_at,
            unpivot.measure,
            unpivot.value
           FROM microclimate.measurements m,
            LATERAL ( VALUES ('air_temp'::text,m.air_temp), ('air_hum'::text,m.air_hum), ('pressure'::text,m.pressure), ('wind_dir'::text,
                        CASE
                            WHEN m.wind_dir::text = 'N'::text THEN 0
                            WHEN m.wind_dir::text = 'NE'::text THEN 45
                            WHEN m.wind_dir::text = 'E'::text THEN 90
                            WHEN m.wind_dir::text = 'SE'::text THEN 135
                            WHEN m.wind_dir::text = 'S'::text THEN 180
                            WHEN m.wind_dir::text = 'SW'::text THEN 225
                            WHEN m.wind_dir::text = 'W'::text THEN 270
                            WHEN m.wind_dir::text = 'NW'::text THEN 315
                            ELSE NULL::integer
                        END), ('wind_impact'::text,m.wind_impact), ('wind_speed'::text,m.wind_speed), ('precip'::text,m.precip), ('sun_irr'::text,m.sun_irr), ('soil_temp'::text,m.soil_temp), ('water_pot'::text,m.water_pot), ('dendro_circ'::text,m.dendro_circ), ('dendro_gain'::text,m.dendro_gain)) unpivot(measure, value)
          WHERE unpivot.value IS NOT NULL
        )
 SELECT q.sensor_id,
    q.measured_at,
    q.point_id,
    q.location_id,
    q.measure,
    q.value,
    q.unit,
    row_number() OVER (PARTITION BY q.point_id, q.measure ORDER BY q.point_id, q.measure, q.measured_at DESC) AS rn
   FROM ( SELECT measurements.sensor_id,
            measurements.measured_at,
            points_heights.point_id,
            points_heights.location_id,
            concat(measurements.measure, points_heights.height_cm) AS measure,
            measurements.value,
            points_heights.unit
           FROM measurements
             JOIN points_heights ON points_heights.sensor_id::text = measurements.sensor_id::text AND points_heights.measure = measurements.measure) q
  ORDER BY q.point_id, q.measure, q.measured_at DESC;

CREATE OR REPLACE VIEW analytic.v_microclimate_measurement_hights
AS SELECT DISTINCT s.sensor_id,
    unpivot.measure,
    unpivot.hight_cm
   FROM microclimate.sensor_devices_import s,
    LATERAL ( VALUES
                    ('avg_temperature'::text,s.air_temp),
                    ('max_temperature'::text,s.air_temp),
                    ('min_temperature'::text,s.air_temp),
                    ('avg_air_hum'::text,s.air_hum),
                    ('avg_pressure'::text,s.pressure),
                    ('max_wind_speed'::text,s.wind_speed),
                    ('avg_wind_speed'::text,s.wind_speed),
                    ('sum_precip'::text,s.precip),
                    ('avg_soil_temp'::text,s.soil_temp),
                    ('avg_water_pot'::text,s.water_pot),
                    ('wind_dir_at_max_speed'::text,s.wind_dir)) unpivot(measure, hight_cm);

CREATE OR REPLACE VIEW analytic.v_microclimate_measurements_daily
AS WITH calendar AS (
         SELECT calendar_1.date,
            sensors_1.sensor_id
           FROM ( SELECT generate_series('2022-08-02 00:00:00+02'::timestamp with time zone, CURRENT_DATE::timestamp with time zone, '1 day'::interval)::date AS date) calendar_1
             CROSS JOIN ( SELECT DISTINCT sensor_devices_import.sensor_id
                   FROM microclimate.sensor_devices_import) sensors_1
        ), first_measurement_per_hour AS (
         SELECT DISTINCT ON (m.sensor_id, (timezone('CET'::text, m.measured_at)::date), (date_part('hour'::text, timezone('CET'::text, m.measured_at)))) m.sensor_id,
            timezone('CET'::text, m.measured_at) AS measured_at,
            timezone('CET'::text, m.measured_at)::date AS date,
            date_part('hour'::text, timezone('CET'::text, m.measured_at)) AS hour,
            m.air_temp,
            m.air_hum,
            m.pressure,
            m.wind_speed,
            m.water_pot,
            m.soil_temp
           FROM microclimate.measurements m
          WHERE date_part('hour'::text, timezone('CET'::text, m.measured_at)) = ANY (ARRAY[7::double precision, 14::double precision, 21::double precision])
          ORDER BY m.sensor_id, (timezone('CET'::text, m.measured_at)::date), (date_part('hour'::text, timezone('CET'::text, m.measured_at))), m.measured_at
        ), seven AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 7::double precision
        ), two AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 14::double precision
        ), nine AS (
         SELECT first_measurement_per_hour.sensor_id,
            first_measurement_per_hour.measured_at,
            first_measurement_per_hour.date,
            first_measurement_per_hour.hour,
            first_measurement_per_hour.air_temp,
            first_measurement_per_hour.air_hum,
            first_measurement_per_hour.pressure,
            first_measurement_per_hour.wind_speed,
            first_measurement_per_hour.water_pot,
            first_measurement_per_hour.soil_temp
           FROM first_measurement_per_hour
          WHERE first_measurement_per_hour.hour = 21::double precision
        ), wind AS (
         SELECT DISTINCT ON (m.sensor_id, (timezone('CET'::text, m.measured_at)::date)) m.sensor_id,
            timezone('CET'::text, m.measured_at)::date AS date,
            m.wind_speed AS max_wind_speed,
                CASE
                    WHEN m.wind_dir::text = 'N'::text THEN 0
                    WHEN m.wind_dir::text = 'NE'::text THEN 45
                    WHEN m.wind_dir::text = 'E'::text THEN 90
                    WHEN m.wind_dir::text = 'SE'::text THEN 135
                    WHEN m.wind_dir::text = 'S'::text THEN 180
                    WHEN m.wind_dir::text = 'SW'::text THEN 225
                    WHEN m.wind_dir::text = 'W'::text THEN 270
                    WHEN m.wind_dir::text = 'NW'::text THEN 315
                    ELSE NULL::integer
                END AS wind_dir_at_max_speed
           FROM microclimate.measurements m
          ORDER BY m.sensor_id, (timezone('CET'::text, m.measured_at)::date), m.wind_speed DESC
        ), min_max_daily_temp AS (
         SELECT m.sensor_id,
            timezone('-04'::text, m.measured_at)::date AS date,
            max(m.air_temp) AS max_temperature,
            min(m.air_temp) AS min_temperature
           FROM microclimate.measurements m
          GROUP BY m.sensor_id, (timezone('-04'::text, m.measured_at)::date)
          ORDER BY m.sensor_id, (timezone('-04'::text, m.measured_at)::date)
        ), precip AS (
         SELECT m.sensor_id,
            timezone('+06'::text, m.measured_at)::date AS date,
            sum(m.precip) AS sum_precip
           FROM microclimate.measurements m
          GROUP BY m.sensor_id, (timezone('+06'::text, m.measured_at)::date)
          ORDER BY m.sensor_id, (timezone('+06'::text, m.measured_at)::date)
        ), calc AS (
         SELECT sensors.address,
            sensors.sensor_position_detail,
            sensors.point_id,
            sensors.air_temp,
            sensors.air_hum,
            sensors.pressure,
            sensors.precip,
            sensors.wind_dir,
            sensors.wind_impact,
            sensors.wind_speed,
            sensors.sun_irr,
            sensors.soil_temp,
            sensors.water_pot,
            sensors.dendro_circ,
            sensors.dendro_gain,
            calendar.date,
            calendar.sensor_id,
            (seven.air_temp + two.air_temp + 2::double precision * nine.air_temp) / 4::double precision AS avg_temperature,
            (seven.air_hum + two.air_hum + nine.air_hum) / 3::double precision AS avg_air_hum,
            (seven.pressure + two.pressure + nine.pressure) / 3 / 100 AS avg_pressure,
            (seven.wind_speed + two.wind_speed + nine.wind_speed) / 3::double precision AS avg_wind_speed,
            (seven.water_pot + two.water_pot + nine.water_pot) / 3::double precision AS avg_water_pot,
            (seven.soil_temp + two.soil_temp + nine.soil_temp) / 3::double precision AS avg_soil_temp,
            wind.max_wind_speed,
            wind.wind_dir_at_max_speed,
            min_max_daily_temp.max_temperature,
            min_max_daily_temp.min_temperature,
            precip.sum_precip
           FROM calendar
             LEFT JOIN seven ON calendar.sensor_id::text = seven.sensor_id::text AND calendar.date = seven.date
             LEFT JOIN two ON calendar.sensor_id::text = two.sensor_id::text AND calendar.date = two.date
             LEFT JOIN nine ON calendar.sensor_id::text = nine.sensor_id::text AND calendar.date = nine.date
             LEFT JOIN wind ON calendar.sensor_id::text = wind.sensor_id::text AND calendar.date = wind.date
             LEFT JOIN min_max_daily_temp ON calendar.sensor_id::text = min_max_daily_temp.sensor_id::text AND calendar.date = min_max_daily_temp.date
             LEFT JOIN precip ON calendar.sensor_id::text = precip.sensor_id::text AND calendar.date = precip.date
             LEFT JOIN microclimate.sensor_devices_import sensors ON calendar.sensor_id::text = sensors.sensor_id::text
        )
 SELECT calc.address,
    calc.sensor_position_detail,
    calc.date,
    calc.sensor_id,
    calc.point_id,
    unpivot.metrika,
    unpivot.value
   FROM calc,
    LATERAL ( VALUES ('avg_temperature'::text,calc.avg_temperature), ('avg_air_hum'::text,calc.avg_air_hum), ('avg_pressure'::text,calc.avg_pressure), ('avg_wind_speed'::text,calc.avg_wind_speed), ('avg_water_pot'::text,calc.avg_water_pot), ('avg_soil_temp'::text,calc.avg_soil_temp), ('max_wind_speed'::text,calc.max_wind_speed), ('max_temperature'::text,calc.max_temperature), ('min_temperature'::text,calc.min_temperature), ('sum_precip'::text,calc.sum_precip), ('wind_dir_at_max_speed'::text,calc.wind_dir_at_max_speed)) unpivot(metrika, value)
  WHERE unpivot.value IS NOT NULL;

CREATE OR REPLACE VIEW analytic.v_microclimate_raw_data
AS WITH tmp_hights AS (
         SELECT DISTINCT s.sensor_id,
            s.address,
            s.sensor_position_detail,
            unpivot.measure,
            unpivot.hight_cm
           FROM microclimate.sensor_devices_import s,
            LATERAL ( VALUES ('Teplota vzduchu'::text,s.air_temp), ('Vlhkost vzduchu'::text,s.air_hum), ('Tlak vzduchu'::text,s.pressure), ('Náraz a směr větru'::text,s.wind_impact), ('Rychlost a směr větru'::text,s.wind_speed), ('Úhrn srážek'::text,s.precip), ('Sluneční svit'::text,s.sun_irr), ('Vodní potenciál půdy'::text,s.water_pot), ('Teplota půdy'::text,s.soil_temp), ('Obvod stromu'::text,s.dendro_circ), ('Přírůstek obvodu stromu'::text,s.dendro_gain)) unpivot(measure, hight_cm)
          WHERE unpivot.hight_cm IS NOT NULL
        ), tmp_measures AS (
         SELECT m_1.sensor_id,
            m_1.measured_at,
            m_1.measured_at::date AS measured_date,
            unpivot.measure_name,
            unpivot.measure,
                CASE
                    WHEN m_1.wind_dir::text = 'N'::text THEN 0
                    WHEN m_1.wind_dir::text = 'NE'::text THEN 45
                    WHEN m_1.wind_dir::text = 'E'::text THEN 90
                    WHEN m_1.wind_dir::text = 'SE'::text THEN 135
                    WHEN m_1.wind_dir::text = 'S'::text THEN 180
                    WHEN m_1.wind_dir::text = 'SW'::text THEN 225
                    WHEN m_1.wind_dir::text = 'W'::text THEN 270
                    WHEN m_1.wind_dir::text = 'NW'::text THEN 315
                    ELSE NULL::integer
                END AS wind_dir
           FROM microclimate.measurements m_1,
            LATERAL ( VALUES ('Teplota vzduchu'::text,m_1.air_temp), ('Vlhkost vzduchu'::text,m_1.air_hum), ('Tlak vzduchu'::text,m_1.pressure), ('Náraz a směr větru'::text,m_1.wind_impact), ('Rychlost a směr větru'::text,m_1.wind_speed), ('Úhrn srážek'::text,m_1.precip), ('Sluneční svit'::text,m_1.sun_irr), ('Vodní potenciál půdy'::text,m_1.water_pot), ('Teplota půdy'::text,m_1.soil_temp), ('Obvod stromu'::text,m_1.dendro_circ), ('Přírůstek obvodu stromu'::text,m_1.dendro_gain)) unpivot(measure_name, measure)
          WHERE m_1.measured_at::date >= (CURRENT_DATE - 7)
        )
 SELECT m.sensor_id,
    t.address,
    t.sensor_position_detail,
    t.hight_cm,
    m.measured_at,
    m.measured_at::date AS measured_date,
    m.measure_name,
        CASE
            WHEN m.measure_name = 'Teplota vzduchu'::text THEN '°C'::text
            WHEN m.measure_name = 'Vlhkost vzduchu'::text THEN '%'::text
            WHEN m.measure_name = 'Tlak vzduchu'::text THEN 'Pa'::text
            WHEN m.measure_name = 'Náraz a směr větru'::text THEN 'km/h'::text
            WHEN m.measure_name = 'Rychlost a směr větru'::text THEN 'km/h'::text
            WHEN m.measure_name = 'Úhrn srážek'::text THEN 'mm'::text
            WHEN m.measure_name = 'Sluneční svit'::text THEN 'lux'::text
            WHEN m.measure_name = 'Vodní potenciál půdy'::text THEN 'kPa'::text
            WHEN m.measure_name = 'Teplota půdy'::text THEN '°C'::text
            WHEN m.measure_name = 'Obvod stromu'::text THEN 'mm'::text
            WHEN m.measure_name = 'Přírůstek obvodu stromu'::text THEN 'µm'::text
            ELSE NULL::text
        END AS unit,
    m.measure,
        CASE
            WHEN m.measure_name = 'Náraz a směr větru'::text THEN m.wind_dir
            WHEN m.measure_name = 'Rychlost a směr větru'::text THEN m.wind_dir
            ELSE NULL::integer
        END AS measure_wind_dir
   FROM tmp_hights t
     JOIN tmp_measures m ON t.sensor_id::text = m.sensor_id::text AND t.measure = m.measure_name;

CREATE OR REPLACE VIEW analytic.v_microclimate_sensor_devices
AS SELECT sdi.sensor_id,
    sdi.location_id,
    sdi.point_id,
    sdi.location,
    sdi.loc_description,
    sdi.loc_orientation,
    sdi.loc_surface,
    sdi.point_name,
    sdi.address,
    sdi.sensor_position,
    sdi.sensor_position_detail,
    sdi.lat,
    sdi.lng,
    ('https://storage.golemio.cz/oict-mikroklima/'::text || sdi.point_id::text) || '.jpg'::text AS url_foto
   FROM microclimate.sensor_devices_import sdi;

CREATE OR REPLACE VIEW analytic.v_microclimate_measurements_daily_w_hights
AS SELECT m.date,
    m.sensor_id,
    m.address,
    m.sensor_position_detail,
    h.measure,
    m.value,
    h.hight_cm,
        CASE
            WHEN h.measure = 'wind_dir_at_max_speed'::text THEN 'Směr maximální rychlosti větru'::text
            WHEN h.measure = 'sum_precip'::text THEN 'Denní úhrn srážek'::text
            WHEN h.measure = 'avg_temperature'::text THEN 'Průměrná denní teplota vzduchu'::text
            WHEN h.measure = 'max_wind_speed'::text THEN 'Maximální rychlost větru'::text
            WHEN h.measure = 'avg_water_pot'::text THEN 'Průměrná denní vlhkost půdy'::text
            WHEN h.measure = 'avg_wind_speed'::text THEN 'Průměrná denní rychlost větru'::text
            WHEN h.measure = 'avg_air_hum'::text THEN 'Průměrná denní relativní vlhkost vzduchu'::text
            WHEN h.measure = 'avg_pressure'::text THEN 'Průměrný denní tlak vzduchu'::text
            WHEN h.measure = 'max_temperature'::text THEN 'Maximální denní teplota vzduchu'::text
            WHEN h.measure = 'avg_soil_temp'::text THEN 'Průměrná denní teplota půdy'::text
            WHEN h.measure = 'min_temperature'::text THEN 'Minimální denní teplota vzduchu'::text
            ELSE NULL::text
        END AS measure_cz,
        CASE
            WHEN h.measure = 'wind_dir_at_max_speed'::text THEN 'm/s'::text
            WHEN h.measure = 'sum_precip'::text THEN 'mm'::text
            WHEN h.measure = 'avg_temperature'::text THEN '°C'::text
            WHEN h.measure = 'max_wind_speed'::text THEN 'm/s'::text
            WHEN h.measure = 'avg_water_pot'::text THEN 'kPa'::text
            WHEN h.measure = 'avg_wind_speed'::text THEN 'm/s'::text
            WHEN h.measure = 'avg_air_hum'::text THEN '%'::text
            WHEN h.measure = 'avg_pressure'::text THEN 'hPa'::text
            WHEN h.measure = 'max_temperature'::text THEN '°C'::text
            WHEN h.measure = 'avg_soil_temp'::text THEN '°C'::text
            WHEN h.measure = 'min_temperature'::text THEN '°C'::text
            ELSE NULL::text
        END AS unit
   FROM analytic.v_microclimate_measurements_daily m
     LEFT JOIN analytic.v_microclimate_measurement_hights h ON m.sensor_id::text = h.sensor_id::text AND m.metrika = h.measure
  ORDER BY m.sensor_id, m.date;

CREATE OR REPLACE VIEW analytic.v_microclimate_wind_dir_at_max_speed
AS WITH max_wind_speed AS (
         SELECT DISTINCT ON (v_microclimate_measurements_daily_w_hights.sensor_id, (date_trunc('month'::text, v_microclimate_measurements_daily_w_hights.date::timestamp with time zone))) v_microclimate_measurements_daily_w_hights.date,
            v_microclimate_measurements_daily_w_hights.sensor_id,
            v_microclimate_measurements_daily_w_hights.value AS wind_speed
           FROM analytic.v_microclimate_measurements_daily_w_hights
          WHERE v_microclimate_measurements_daily_w_hights.measure = 'max_wind_speed'::text
          ORDER BY v_microclimate_measurements_daily_w_hights.sensor_id, (date_trunc('month'::text, v_microclimate_measurements_daily_w_hights.date::timestamp with time zone)), v_microclimate_measurements_daily_w_hights.value DESC
        ), dir_wind_speed AS (
         SELECT vmma.date,
            vmma.sensor_id,
            vmma.value AS wind_dir
           FROM analytic.v_microclimate_measurements_daily_w_hights vmma
          WHERE vmma.measure = 'wind_dir_at_max_speed'::text AND vmma.value IS NOT NULL
        )
 SELECT max_wind_speed.date,
    max_wind_speed.wind_speed,
    max_wind_speed.sensor_id,
    dir_wind_speed.wind_dir AS dir
   FROM max_wind_speed
     JOIN dir_wind_speed ON max_wind_speed.sensor_id::text = dir_wind_speed.sensor_id::text AND max_wind_speed.date = dir_wind_speed.date
  ORDER BY max_wind_speed.date;
