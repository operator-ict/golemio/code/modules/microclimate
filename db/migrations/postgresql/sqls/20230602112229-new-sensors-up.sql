DROP VIEW analytic.v_microclimate_wind_dir_at_max_speed;

DROP VIEW analytic.v_microclimate_measurements_daily_w_hights;

DROP VIEW analytic.v_microclimate_measurement_hights;

DROP VIEW analytic.v_microclimate_measurements_daily;

DROP VIEW analytic.v_microclimate_raw_data;

DROP VIEW analytic.v_microclimate_sensor_devices;

DROP VIEW microclimate.v_sensor_points;

DROP VIEW microclimate.v_sensor_measurements;

DROP TABLE microclimate.sensor_devices_import;

CREATE TABLE microclimate.sensor_devices_import (
	location_id int4 NULL,
	point_id int4 NULL,
	"location" varchar(50) NULL,
	loc_description varchar(50) NULL,
	loc_orientation varchar(50) NULL,
	loc_surface varchar(50) NULL,
	point_name varchar(50) NULL,
	point_whole_name varchar(50) NULL,
	address varchar(50) NULL,
	sensor_position varchar(50) NULL,
	sensor_position_detail varchar(50) NULL,
	sensor_id varchar(50) NOT NULL,
	data_relevance varchar(50) NULL,
	lat float4 NULL,
	lng float4 NULL,
	air_temp int4 NULL,
	air_hum int4 NULL,
	pressure int4 NULL,
	wind_dir int4 NULL,
	wind_impact int4 NULL,
	wind_speed int4 NULL,
	precip int4 NULL,
	sun_irr int4 NULL,
	soil_temp int4 NULL,
	water_pot int4 NULL,
	dendro_circ int4 NULL,
	dendro_gain int4 NULL,
    data_until varchar(50) NULL,
	CONSTRAINT sensor_devices_import_pkey PRIMARY KEY (sensor_id)
);

CREATE OR REPLACE VIEW microclimate.v_sensor_points
AS SELECT DISTINCT s.point_id,
    s.point_name,
    s.location_id,
    s.location,
    s.loc_description,
    s.loc_orientation,
    s.loc_surface,
    s.lat,
    s.lng,
    NULL::text AS x_jtsk,
    NULL::text AS y_jtsk,
    NULL::text AS elevation_m,
    s.sensor_position,
    s.sensor_position_detail,
    concat(unpivot.measure, unpivot.height_cm) AS measure,
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN 'Teplota vzduchu'::text
            WHEN unpivot.measure = 'air_hum'::text THEN 'Vlhkost vzduchu'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Atmosférický tlak'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN 'Směr větru'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'Náraz větru'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'Rychlost větru'::text
            WHEN unpivot.measure = 'precip'::text THEN 'Úhrn srážek'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'Sluneční záření'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN 'Teplota půdy'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'Vodní potenciál půdy'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'Obvod stromu'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'Přírůstek obvodu stromu'::text
            ELSE NULL::text
        END AS measure_cz,
        CASE
            WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
            WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
            WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
            WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
            WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
            WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
            WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
            WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
            WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
            WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
            ELSE NULL::text
        END AS unit
   FROM microclimate.sensor_devices_import s,
    LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
  WHERE unpivot.height_cm IS NOT NULL
  ORDER BY s.point_id;

CREATE OR REPLACE VIEW microclimate.v_sensor_measurements
AS WITH points_heights AS (
         SELECT s.sensor_id,
            s.point_id,
            s.location_id,
            unpivot.measure,
            unpivot.height_cm,
                CASE
                    WHEN unpivot.measure = 'air_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'air_hum'::text THEN '%'::text
                    WHEN unpivot.measure = 'pressure'::text THEN 'Pa'::text
                    WHEN unpivot.measure = 'wind_dir'::text THEN '°'::text
                    WHEN unpivot.measure = 'wind_impact'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'wind_speed'::text THEN 'km/h'::text
                    WHEN unpivot.measure = 'precip'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'sun_irr'::text THEN 'lux'::text
                    WHEN unpivot.measure = 'soil_temp'::text THEN '°C'::text
                    WHEN unpivot.measure = 'water_pot'::text THEN 'kPa'::text
                    WHEN unpivot.measure = 'dendro_circ'::text THEN 'mm'::text
                    WHEN unpivot.measure = 'dendro_gain'::text THEN 'µm'::text
                    ELSE NULL::text
                END AS unit
           FROM microclimate.sensor_devices_import s,
            LATERAL ( VALUES ('air_temp'::text,s.air_temp), ('air_hum'::text,s.air_hum), ('pressure'::text,s.pressure), ('wind_dir'::text,s.wind_dir), ('wind_impact'::text,s.wind_impact), ('wind_speed'::text,s.wind_speed), ('precip'::text,s.precip), ('sun_irr'::text,s.sun_irr), ('soil_temp'::text,s.soil_temp), ('water_pot'::text,s.water_pot), ('dendro_circ'::text,s.dendro_circ), ('dendro_gain'::text,s.dendro_gain)) unpivot(measure, height_cm)
          WHERE unpivot.height_cm IS NOT NULL
        ), measurements AS (
         SELECT m.sensor_id,
            m.measured_at,
            unpivot.measure,
            unpivot.value
           FROM microclimate.measurements m,
            LATERAL ( VALUES ('air_temp'::text,m.air_temp), ('air_hum'::text,m.air_hum), ('pressure'::text,m.pressure), ('wind_dir'::text,
                        CASE
                            WHEN m.wind_dir::text = 'N'::text THEN 0
                            WHEN m.wind_dir::text = 'NE'::text THEN 45
                            WHEN m.wind_dir::text = 'E'::text THEN 90
                            WHEN m.wind_dir::text = 'SE'::text THEN 135
                            WHEN m.wind_dir::text = 'S'::text THEN 180
                            WHEN m.wind_dir::text = 'SW'::text THEN 225
                            WHEN m.wind_dir::text = 'W'::text THEN 270
                            WHEN m.wind_dir::text = 'NW'::text THEN 315
                            ELSE NULL::integer
                        END), ('wind_impact'::text,m.wind_impact), ('wind_speed'::text,m.wind_speed), ('precip'::text,m.precip), ('sun_irr'::text,m.sun_irr), ('soil_temp'::text,m.soil_temp), ('water_pot'::text,m.water_pot), ('dendro_circ'::text,m.dendro_circ), ('dendro_gain'::text,m.dendro_gain)) unpivot(measure, value)
          WHERE unpivot.value IS NOT NULL
        )
 SELECT q.sensor_id,
    q.measured_at,
    q.point_id,
    q.location_id,
    q.measure,
    q.value,
    q.unit,
    row_number() OVER (PARTITION BY q.point_id, q.measure ORDER BY q.point_id, q.measure, q.measured_at DESC) AS rn
   FROM ( SELECT measurements.sensor_id,
            measurements.measured_at,
            points_heights.point_id,
            points_heights.location_id,
            concat(measurements.measure, points_heights.height_cm) AS measure,
            measurements.value,
            points_heights.unit
           FROM measurements
             JOIN points_heights ON points_heights.sensor_id::text = measurements.sensor_id::text AND points_heights.measure = measurements.measure) q
  ORDER BY q.point_id, q.measure, q.measured_at DESC;
