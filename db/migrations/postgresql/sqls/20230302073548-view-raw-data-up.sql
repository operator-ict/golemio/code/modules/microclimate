DROP VIEW analytic.v_microclimate_sensor_devices;

CREATE OR REPLACE VIEW analytic.v_microclimate_sensor_devices
AS SELECT sdi.sensor_id,
    sdi.location_id,
    sdi.point_id,
    sdi.location,
    sdi.loc_description,
    sdi.loc_orientation,
    sdi.loc_surface,
    sdi.point_name,
    sdi.address,
    sdi.sensor_position,
    sdi.sensor_position_detail,
    sdi.lat,
    sdi.lng,
    ('https://storage.golemio.cz/oict-mikroklima/'::text || sdi.point_id::text) || '.jpg'::text AS url_foto
   FROM microclimate.sensor_devices_import sdi;

CREATE OR REPLACE VIEW analytic.v_microclimate_raw_data
AS WITH tmp_hights AS (
         SELECT DISTINCT s.sensor_id,
            s.address,
            s.sensor_position_detail,
            unpivot.measure,
            unpivot.hight_cm
           FROM microclimate.sensor_devices_import s,
            LATERAL ( VALUES ('Teplota vzduchu'::text,s.air_temp), ('Vlhkost vzduchu'::text,s.air_hum), ('Tlak vzduchu'::text,s.pressure), ('Náraz a směr větru'::text,s.wind_impact), ('Rychlost a směr větru'::text,s.wind_speed), ('Úhrn srážek'::text,s.precip), ('Sluneční svit'::text,s.sun_irr), ('Vodní potenciál půdy'::text,s.water_pot), ('Teplota půdy'::text,s.soil_temp), ('Obvod stromu'::text,s.dendro_circ), ('Přírůstek obvodu stromu'::text,s.dendro_gain)) unpivot(measure, hight_cm)
          WHERE unpivot.hight_cm IS NOT NULL
        ), tmp_measures AS (
         SELECT m_1.sensor_id,
            m_1.measured_at,
            m_1.measured_at::date AS measured_date,
            unpivot.measure_name,
            unpivot.measure,
                CASE
                    WHEN m_1.wind_dir::text = 'N'::text THEN 0
                    WHEN m_1.wind_dir::text = 'NE'::text THEN 45
                    WHEN m_1.wind_dir::text = 'E'::text THEN 90
                    WHEN m_1.wind_dir::text = 'SE'::text THEN 135
                    WHEN m_1.wind_dir::text = 'S'::text THEN 180
                    WHEN m_1.wind_dir::text = 'SW'::text THEN 225
                    WHEN m_1.wind_dir::text = 'W'::text THEN 270
                    WHEN m_1.wind_dir::text = 'NW'::text THEN 315
                    ELSE NULL::integer
                END AS wind_dir
           FROM microclimate.measurements m_1,
            LATERAL ( VALUES ('Teplota vzduchu'::text,m_1.air_temp), ('Vlhkost vzduchu'::text,m_1.air_hum), ('Tlak vzduchu'::text,m_1.pressure), ('Náraz a směr větru'::text,m_1.wind_impact), ('Rychlost a směr větru'::text,m_1.wind_speed), ('Úhrn srážek'::text,m_1.precip), ('Sluneční svit'::text,m_1.sun_irr), ('Vodní potenciál půdy'::text,m_1.water_pot), ('Teplota půdy'::text,m_1.soil_temp), ('Obvod stromu'::text,m_1.dendro_circ), ('Přírůstek obvodu stromu'::text,m_1.dendro_gain)) unpivot(measure_name, measure)
          WHERE m_1.measured_at::date >= (CURRENT_DATE - 7)
        )
 SELECT m.sensor_id,
    t.address,
    t.sensor_position_detail,
    t.hight_cm,
    m.measured_at,
    m.measured_at::date AS measured_date,
    m.measure_name,
        CASE
            WHEN m.measure_name = 'Teplota vzduchu'::text THEN '°C'::text
            WHEN m.measure_name = 'Vlhkost vzduchu'::text THEN '%'::text
            WHEN m.measure_name = 'Tlak vzduchu'::text THEN 'Pa'::text
            WHEN m.measure_name = 'Náraz a směr větru'::text THEN 'km/h'::text
            WHEN m.measure_name = 'Rychlost a směr větru'::text THEN 'km/h'::text
            WHEN m.measure_name = 'Úhrn srážek'::text THEN 'mm'::text
            WHEN m.measure_name = 'Sluneční svit'::text THEN 'lux'::text
            WHEN m.measure_name = 'Vodní potenciál půdy'::text THEN 'kPa'::text
            WHEN m.measure_name = 'Teplota půdy'::text THEN '°C'::text
            WHEN m.measure_name = 'Obvod stromu'::text THEN 'mm'::text
            WHEN m.measure_name = 'Přírůstek obvodu stromu'::text THEN 'µm'::text
            ELSE NULL::text
        END AS unit,
    m.measure,
        CASE
            WHEN m.measure_name = 'Náraz a směr větru'::text THEN m.wind_dir
            WHEN m.measure_name = 'Rychlost a směr větru'::text THEN m.wind_dir
            ELSE NULL::integer
        END AS measure_wind_dir
   FROM tmp_hights t
     JOIN tmp_measures m ON t.sensor_id::text = m.sensor_id::text AND t.measure = m.measure_name;
