import amqplib from "amqplib";
import fs from "fs";
import path from "path";

const RABBIT_CONN = ``;
const RABBIT_EXCHANGE_NAME = `dataplatform`;
const RABBIT_EXCHANGE_NAME_ALT = `dataplatform-alt`;
const RABBIT_QUEUE_NAME = "workers.dataplatform.microclimate.refreshMeasurementsById";

const sensorIds = [
    "323434316A317019",
    "323434316A317519",
    "323434316D317B18",
    "323434316D317918",
    "323434316D318A18",
    "323434316D318C18",
    "323434316D318418",
    "323434316F316B18",
    "323434316F316E18",
    "323434316F317A18",
    "323434316F317E18",
    "323434316F317218",
    "323434316F317418",
    "323434316F317518",
    "323434316F317818",
    "323434317E318718",
    "323434317F317F18",
    "323434317F317618",
    "323434317F317818",
    "323434317F317918",
    "323434317F318518",
    "3234343173315F18",
    "3234343173316118",
    "3234343173316218",
    "3234343173316418",
    "3234343173316818",
    "3234343173317C18",
    "3234343173317D18",
    "3234343173317418",
    "3234343174315F18",
    "3234343174316E18",
    "3234343174316018",
    "3234343174316118",
    "3234343174316218",
    "3234343174317118",
    "3234343174317218",
    "3234343174317318",
    "3234343174317618",
    "3234343174317718",
    "3234343176316A18",
    "3234343176316E18",
    "3234343176316918",
    "3234343176317018",
    "3234343176317918",
    "3234343176318A18",
    "3234343176318B18",
    "3234343176318318",
    "3234343176318418",
    "3234343176319018",
    "3234343176319118",
    "3234343177316C18",
    "3234343177316318",
    "3234343177316418",
    "3234343177316718",
    "3234343177316918",
    "3234343177317018",
    "3234343177317318",
    "3234343177318F18",
    "3234343177319018",
    "3234343178316418",
    "3234343178317118",
    "3234343178317218",
    "3234343178317518",
    "3234343178318C18",
    "3234343178318218",
    "3234343178318318",
    "3234343178318818",
    "3234343179318218",
    "3234343179318318",
    "3234343179318418",
    "3234343179318918",
    "3234343180316B18",
    "3234343180316C18",
    "3234343180316E18",
    "3234343180316F18",
];

(async () => {
    const connection = await amqplib.connect(RABBIT_CONN);
    const channel = await connection.createChannel();

    await channel.assertExchange(RABBIT_EXCHANGE_NAME, "topic", {
        durable: false,
        alternateExchange: RABBIT_EXCHANGE_NAME_ALT,
    });

    const sendMessage = async (key: string, msg: string, options: object = {}): Promise<boolean> => {
        try {
            return channel.publish(RABBIT_EXCHANGE_NAME as string, key, Buffer.from(msg), { expiration: 240 * 60 * 1000 });
        } catch (error) {
            console.log(error);
            return false;
        }
    };

    try {
        for (const id of sensorIds) {
            const message1 = {
                sensorAddr: id,
                dateRange: "previousYear",
            };
            const message2 = {
                sensorAddr: id,
                dateRange: "currentYear",
            };
            await sendMessage(RABBIT_QUEUE_NAME, JSON.stringify(message1));
            await sendMessage(RABBIT_QUEUE_NAME, JSON.stringify(message2));
        }
    } finally {
        await channel.close();
        await connection.close();
    }
})();
