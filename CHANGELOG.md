# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.8] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#263](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/263))

## [1.2.7] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.6] - 2024-07-17

### Fixed

-   OG validations ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.2.5] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

### Removed

-   remove redis useCacheMiddleware ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

## [1.2.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.3] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.2.2] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.2.1] - 2024-01-15

### Added

-   Add pagination limit for measurements output API
-   Add 10 seconds cache TTL to all API endpoints

### Changed

-   Replace `v_sensor_measurements` with optimized materialized views to improve performance of our API ([microclimate#6](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/issues/6))

## [1.2.0] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.1.11] - 2023-07-31

### Changed

-   adjust v_sensor_points view to contain height in `measure_cz`

## [1.1.10] - 2023-07-17

### Changed

-   change date type in table sensor_devices_import

## [1.1.9] - 2023-06-21

### Changed

-   Change view microclimate.v_sensor_measurements
-   Change date type data_relevance, date_until to date on table microclimate.sensor_devices_import

## [1.1.8] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))
-   Change date type data_relevance, date_until to date on table microclimate.sensor_devices_import

## [1.1.7] - 2023-06-05

### Changed

-   Adding new column data_until to table microclimate.sensor_devices_import, [issue](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/15).

## [1.1.6] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.5] - 2023-05-14

### Changed

-   Update openapi docs

## [1.1.4] - 2023-03-13

### Added

-   Microclimate API ([microclimate#2](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/issues/2))

## [1.1.3] - 2023-03-08

### Changed

-   Creating new view, analytic.v_microclimate_raw_data, and modifying an exsiting one, analytic.v_microclimate_sensor_devices, [issue](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/14)

## [1.1.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-01

### Added

-   changes and adding analytic views ([p0252-mereni-mikroklimatu-meteorologicke-udaje#9](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/9))
-   new analytic views ([p0252-mereni-mikroklimatu-meteorologicke-udaje#9](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/9))

### Fixed

-   conversion of wind direction to degrees in `analytic.v_microclimate_measurements_daily`

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.4] - 2023-01-04

### Fixed

-   dateRange as parameter for MeasurementsDataSourceFactory ([microclimate#1](https://gitlab.com/operator-ict/golemio/code/modules/microclimate))

## [1.0.3] - 2022-12-13

### Added

-   new table sensor_devices_import([p0252-mereni-mikroklimatu-meteorologicke-udaje#5](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/5))

### Fixed

-   Input property from volumemm to rainvolume [p0252-mereni-mikroklimatu-meteorologicke-udaje](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/7)

## [1.0.1] - 2022-11-03

### Added

-   Initial module development [p0252-mereni-mikroklimatu-meteorologicke-udaje](https://gitlab.com/operator-ict/golemio/projekty/oict/p0252-mereni-mikroklimatu-meteorologicke-udaje/-/issues/2)

