import { RefreshSensorViewsTask } from "#ie/workers/tasks/RefreshSensorViewsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("MicroclimateWorker - RefreshSensorViewsTask", () => {
    let sandbox: SinonSandbox;
    let queryStub: SinonStub;
    let releaseStub: SinonStub;
    let logErrorStub: SinonStub;
    let testContainer = container.createChildContainer();

    before(async () => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        sandbox.restore();

        queryStub = sandbox.stub().resolves();
        // workaround for .catch() in finally block
        releaseStub = sandbox.stub().resolves();
        logErrorStub = sandbox.stub();

        testContainer
            .registerSingleton(
                CoreToken.SimpleConfig,
                class DummyConfig {
                    getValue() {
                        return 6969;
                    }
                }
            )
            .registerSingleton(
                CoreToken.Logger,
                class DummyLogger {
                    info = sandbox.stub();
                    error = logErrorStub;
                }
            )
            .registerSingleton(
                ContainerToken.RedisConnector,
                class DummyRedisConnector {
                    async getClient() {
                        return {};
                    }
                }
            )
            .registerSingleton(
                ContainerToken.PostgresConnector,
                class DummyPostgresConnector {
                    getConnection() {
                        return {
                            query: queryStub,
                        };
                    }
                }
            )
            .registerSingleton(RefreshSensorViewsTask);
    });

    afterEach(() => {
        testContainer.reset();
    });

    it("should call queries to refresh measurement views", async () => {
        const task = testContainer.resolve(RefreshSensorViewsTask);
        sandbox.stub(task, "createMutex" as any).returns({
            tryAcquire: sandbox.stub().resolves(true),
            release: releaseStub,
        });

        await task["execute"]();
        expect(queryStub.callCount).to.equal(3);
        expect(releaseStub.callCount).to.equal(1);
    });

    it("should not call queries to refresh measurement views when mutex lock was not acquired", async () => {
        const task = testContainer.resolve(RefreshSensorViewsTask);
        sandbox.stub(task, "createMutex" as any).returns({
            tryAcquire: sandbox.stub().resolves(false),
            release: releaseStub,
        });

        await task["execute"]();
        expect(queryStub.callCount).to.equal(0);
        expect(releaseStub.callCount).to.equal(0);
    });

    it("should throw error when mutex lock acquire fails", async () => {
        const task = testContainer.resolve(RefreshSensorViewsTask);
        sandbox.stub(task, "createMutex" as any).returns({
            tryAcquire: sandbox.stub().rejects(new Error("test error")),
            release: releaseStub,
        });

        await expect(task["execute"]()).to.be.rejectedWith("RefreshSensorViewsTask: error while acquiring mutex lock");
        expect(releaseStub.callCount).to.equal(0);
    });

    it("should release mutex lock when queries fail", async () => {
        const task = testContainer.resolve(RefreshSensorViewsTask);
        sandbox.stub(task, "createMutex" as any).returns({
            tryAcquire: sandbox.stub().resolves(true),
            release: releaseStub,
        });
        queryStub.onFirstCall().rejects(new Error("test error"));

        await expect(task["execute"]()).to.be.rejectedWith("RefreshSensorViewsTask: error while refreshing sensor views");
        expect(queryStub.callCount).to.equal(1);
        expect(releaseStub.callCount).to.equal(1);
    });

    it("should log error when mutex lock release fails", async () => {
        const task = testContainer.resolve(RefreshSensorViewsTask);
        sandbox.stub(task, "createMutex" as any).returns({
            tryAcquire: sandbox.stub().resolves(true),
            release: sandbox.stub().rejects(new Error("test error")),
        });

        await task["execute"]();
        expect(queryStub.callCount).to.equal(3);
        expect(logErrorStub.callCount).to.equal(1);
        expect(logErrorStub.getCall(0).args[0]).to.equal("RefreshSensorViewsTask: error while releasing mutex: test error");
    });
});
