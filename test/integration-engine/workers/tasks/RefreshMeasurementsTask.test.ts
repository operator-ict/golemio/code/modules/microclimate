/* eslint-disable max-len */
import { ISensorIdObject, RefreshMeasurementsTask } from "#ie/workers/tasks";
import { DateRange, IRefreshMeasurementsByIdTask } from "#sch/Measurements";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { SinonSandbox, SinonSpy, createSandbox } from "sinon";

describe("MicroclimateWorker - RefreshMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshMeasurementsTask;
    let sensorDevicesFromDB: ISensorIdObject[];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sensorDevicesFromDB = [
            { sensor_id: "323434316F317A18" },
            { sensor_id: "423434316F317A19" },
            { sensor_id: "523434316F317A20" },
        ];
        task = new RefreshMeasurementsTask("test.microclimate");

        sandbox.stub(task["repository"], "find").callsFake(() => Promise.resolve(sensorDevicesFromDB));
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
        sandbox.stub(task["microclimateRedisChannel"], "createSubscriber" as any).callsFake(() => ({
            subscribe: sandbox.stub().resolves(),
            listen: sandbox.stub().resolves(),
            unsubscribe: sandbox.stub().resolves(),
        }));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();

        sandbox.assert.calledOnce(task["repository"].find as SinonSpy);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 4);

        for (const data of sensorDevicesFromDB) {
            const msg: IRefreshMeasurementsByIdTask = {
                sensorAddr: data.sensor_id,
                dateRange: DateRange.TODAY,
            };

            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.microclimate",
                "refreshMeasurementsById",
                msg
            );
        }

        sandbox.assert.callOrder(task["repository"].find as SinonSpy, QueueManager["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.calledWith(
            QueueManager["sendMessageToExchange"] as SinonSpy,
            "test.microclimate",
            "refreshSensorViews",
            {}
        );
    });
});
