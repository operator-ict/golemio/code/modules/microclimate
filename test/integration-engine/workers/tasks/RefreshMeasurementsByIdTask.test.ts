/* eslint-disable max-len */
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { RefreshMeasurementsByIdTask } from "#ie/workers/tasks";
import { IMeasurementInput } from "#sch/datasources/MeasurementsJsonSchema";
import { DateRange, IMeasurements, IRefreshMeasurementsByIdTask } from "#sch/Measurements";
import { MeasurementsDataSourceFactory } from "#ie/datasources/MeasurementsDataSourceFactory";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ORIGIN_HOSTNAME_HEADER } from "#ie/const";

describe("MicroclimateWorker - RefreshMeasurementsByIdTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshMeasurementsByIdTask;
    let testData: IMeasurementInput[];
    let testTransformedData: IMeasurements[];
    let msg: IRefreshMeasurementsByIdTask;
    let dataSource: DataSource;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new RefreshMeasurementsByIdTask("test.microclimate");

        msg = { sensorAddr: "323434316F317A18", dateRange: DateRange.TODAY };

        testData = [
            {
                sensor: {
                    type: "unitlv",
                },
                data: {
                    airTemp1: 8.8,
                    airHum1: 96.7,
                    pressure: 99300,
                },
                id: "635318c94b538047df62856d",
                date: "2022-10-21T22:10:17.116Z",
            },
            {
                sensor: {
                    type: "unitlv",
                },
                data: {
                    airTemp1: 8.7,
                    airHum1: 97.6,
                    pressure: 99290,
                },
                id: "63531b214b538047df62888f",
                date: "2022-10-21T22:20:17.129Z",
            },
        ];

        testTransformedData = [
            {
                air_hum: 96.7,
                air_temp: 8.8,
                measured_at: "2022-10-21T22:10:17.116Z",
                measurement_id: "635318c94b538047df62856d",
                pressure: 99300,
                sensor_id: "323434316F317A18",
            },
            {
                air_hum: 97.6,
                air_temp: 8.7,
                measured_at: "2022-10-21T22:20:17.129Z",
                measurement_id: "63531b214b538047df62888f",
                pressure: 99290,
                sensor_id: "323434316F317A18",
            },
        ];

        dataSource = MeasurementsDataSourceFactory.getDataSource(msg);
        sandbox.stub(MeasurementsDataSourceFactory, "getDataSource").withArgs(msg).returns(dataSource);
        sandbox.stub(dataSource, "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
        sandbox.stub(task["microclimateRedisChannel"], "publishMessage" as any).resolves();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"](msg, { headers: { [ORIGIN_HOSTNAME_HEADER]: "test-suffix" } } as any);

        sandbox.assert.calledOnce(MeasurementsDataSourceFactory.getDataSource as SinonSpy);
        sandbox.assert.calledOnce(dataSource.getAll as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData, msg.sensorAddr);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);
        sandbox.assert.calledWith(task["microclimateRedisChannel"].publishMessage as SinonSpy, "OK", {
            channelSuffix: "test-suffix",
        });

        sandbox.assert.callOrder(
            MeasurementsDataSourceFactory.getDataSource as SinonSpy,
            dataSource.getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveBulk as SinonSpy
        );
    });
});
