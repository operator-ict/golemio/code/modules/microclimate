/* eslint-disable max-len */
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { RefreshSensorDevicesTask } from "#ie/workers/tasks";

import { ISensorDevice } from "#sch/SensorDevices";
import { IDeviceInput } from "#sch/datasources/SensorDevicesJsonSchema";

describe("MicroclimateWorker - RefreshSensorDevicesTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshSensorDevicesTask;
    let testData: IDeviceInput[];
    let testTransformedData: ISensorDevice[];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new RefreshSensorDevicesTask("test.microclimate");

        testData = [
            {
                name: "Tržnice | střed | VO | 1m | #063",
                address: "323434316F317A18",
                location: {
                    lat: 50.0989344,
                    lng: 14.4450222,
                },
            },
        ];

        testTransformedData = [
            {
                whole_name: "Tržnice | střed | VO | 1m | #063",
                sensor_id: "323434316F317A18",
                coordinate_lat: 50.0989344,
                coordinate_lng: 14.4450222,
            },
        ];

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();

        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveBulk as SinonSpy
        );
    });
});
