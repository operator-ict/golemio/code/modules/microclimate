import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";

import { MicroclimateWorker } from "#ie/workers/MicroclimateWorker";

describe("MicroclimateWorker", () => {
    let sandbox: SinonSandbox;
    let worker: MicroclimateWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        worker = new MicroclimateWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".microclimate");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("Microclimate");
            expect(result.queues.length).to.equal(4);
        });
    });

    describe("registerTask", () => {
        it("should have 3 tasks registered", () => {
            expect(worker["queues"].length).to.equal(4);
            expect(worker["queues"][0].name).to.equal("refreshSensorViews");
            expect(worker["queues"][1].name).to.equal("refreshSensorDevices");
            expect(worker["queues"][2].name).to.equal("refreshMeasurements");
            expect(worker["queues"][3].name).to.equal("refreshMeasurementsById");
            expect(worker["queues"][0].options.messageTtl).to.equal(600000);
            expect(worker["queues"][1].options.messageTtl).to.equal(82800000);
            expect(worker["queues"][2].options.messageTtl).to.equal(82800000);
        });
    });
});
