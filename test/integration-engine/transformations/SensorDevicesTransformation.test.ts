import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { SensorDevicesTransformation } from "#ie/transformations/SensorDevicesTransformation";

chai.use(chaiAsPromised);

describe("SensorDevicesTransformation", () => {
    let transformation: SensorDevicesTransformation;
    let testSourceData: any[];

    const transformedDataFixture = {
        coordinate_lat: 50.0989344,
        coordinate_lng: 14.4450222,
        sensor_id: "323434316F317A18",
        whole_name: "Tržnice | střed | VO | 1m | #063",
    };

    beforeEach(async () => {
        transformation = new SensorDevicesTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/sensor-devices-datasource.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SensorDevices");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (const item of data) {
            expect(item).to.have.property("whole_name");
            expect(item).to.have.property("sensor_id");
        }
    });
});
