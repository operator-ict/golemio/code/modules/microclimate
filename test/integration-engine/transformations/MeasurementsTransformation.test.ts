import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { MeasurementsTransformation } from "#ie/transformations/MeasurementsTransformation";

chai.use(chaiAsPromised);

describe("MeasurementsTransformation", () => {
    let transformation: MeasurementsTransformation;
    let testSourceData: any;
    let sensorId: string;

    const transformedDataFixture = {
        air_hum: 97.2,
        air_temp: 8.9,
        measured_at: "2022-10-21T22:00:17.107Z",
        measurement_id: "635316714b538047df62823c",
        pressure: 99300,
        sensor_id: "323434316F317A18",

        // properties with no data:
        dendro_circ: undefined,
        dendro_gain: undefined,
        precip: undefined,
        soil_temp: undefined,
        sun_irr: undefined,
        water_pot: undefined,
        wind_dir: undefined,
        wind_impact: undefined,
        wind_speed: undefined,
    };

    beforeEach(async () => {
        transformation = new MeasurementsTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/measurements-datasource.json", "utf8"));
        sensorId = "323434316F317A18";
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("Measurements");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transform"](testSourceData, sensorId);
        expect(data[0]).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData, sensorId);
        for (const item of data) {
            expect(item).to.have.property("sensor_id");
            expect(item).to.have.property("measurement_id");
            expect(item).to.have.property("measured_at");
        }
    });
});
