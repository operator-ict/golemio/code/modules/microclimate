import sinon from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { microclimateRouter } from "#og";
import { microclimateLocationsResponse } from "./data/microclimate-locations-response";
import { microclimatePointsResponse } from "./data/microclimate-points-response";
import { microclimateMeasurementsResponse } from "./data/microclimate-measurements-response";
import { microclimateMeasurementsUniqueResponse } from "./data/microclimate-measurements-unique-response";

chai.use(chaiAsPromised);

describe("Microclimate Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        app.use("/microclimate", microclimateRouter);
        app.use((err: any, req: Request, res: Response, _: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /microclimate/locations", (done) => {
        request(app)
            .get("/microclimate/locations?locationId=130")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.equal(microclimateLocationsResponse);
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /microclimate/points", (done) => {
        request(app)
            .get("/microclimate/points?locationId=130&pointId=132")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(microclimatePointsResponse);
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /microclimate/measurements (unique measurements for point_id measure pair)", (done) => {
        request(app)
            .get("/microclimate/measurements?locationId=130&pointId=131")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(microclimateMeasurementsUniqueResponse);
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /microclimate/measurements", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                "/microclimate/measurements?locationId=130&pointId=131&measure=air_temp50&from=2023-02-16T09:00:00.000Z&to=2023-02-16T09:30:00.000Z&limit=2&offset=1"
            )
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(microclimateMeasurementsResponse);
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((err) => done(err));
    });
});
