import { ILocationOutput } from "#og/helpers/LocationsOutputMapper";

export const microclimateLocationsResponse: ILocationOutput[] = [
    {
        location_id: 130,
        location: "Pražská Holešovická tržnice",
        loc_description: "areál bez zeleně",
        loc_orientation: "východ-západ",
        loc_surface: "asfalt/beton",
        address: "Pražská tržnice",
        points: [
            {
                point_id: 131,
                point_name: "Pražská tržnice osvětlení",
            },
            {
                point_id: 132,
                point_name: "Pražská tržnice jižní strom",
            },
            {
                point_id: 133,
                point_name: "Pražská tržnice severní strom",
            },
        ],
    },
];
