import { ISensorPointsViewOutput } from "#og/helpers/PointsOutputMapper";

export const microclimatePointsResponse: ISensorPointsViewOutput[] = [
    {
        point_id: 132,
        location_id: 130,
        point_name: "Pražská tržnice jižní strom",
        location: "Pražská Holešovická tržnice",
        loc_description: "areál bez zeleně",
        loc_orientation: "východ-západ",
        loc_surface: "asfalt/beton",
        lat: 50.098816,
        lng: 14.445016,
        x_jtsk: null,
        y_jtsk: null,
        elevation_m: null,
        measures: [
            {
                measure: "air_hum50",
                measure_cz: "Vlhkost vzduchu, 50 cm",
                unit: "%",
            },
            {
                measure: "air_temp50",
                measure_cz: "Teplota vzduchu, 50 cm",
                unit: "°C",
            },
            {
                measure: "dendro_circ200",
                measure_cz: "Obvod stromu, 200 cm",
                unit: "mm",
            },
            {
                measure: "dendro_gain200",
                measure_cz: "Přírůstek obvodu stromu, 200 cm",
                unit: "µm",
            },
            {
                measure: "pressure50",
                measure_cz: "Atmosférický tlak, 50 cm",
                unit: "Pa",
            },
            {
                measure: "soil_temp-10",
                measure_cz: "Teplota půdy, -10 cm",
                unit: "°C",
            },
            {
                measure: "soil_temp-30",
                measure_cz: "Teplota půdy, -30 cm",
                unit: "°C",
            },
            {
                measure: "water_pot-10",
                measure_cz: "Vodní potenciál půdy, -10 cm",
                unit: "kPa",
            },
            {
                measure: "water_pot-30",
                measure_cz: "Vodní potenciál půdy, -30 cm",
                unit: "kPa",
            },
        ],
        sensor_position: "strom",
        sensor_position_detail: "jižní strom",
    },
];
