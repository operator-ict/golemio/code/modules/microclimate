import { ISensorMeasurementsView } from "#sch/SensorMeasurementsView";

export const microclimateMeasurementsResponse: ISensorMeasurementsView[] = [
    {
        measured_at: "2023-02-16T09:13:41.643Z",
        point_id: 131,
        location_id: 130,
        measure: "air_temp50",
        value: 1.9,
        unit: "°C",
    },
    {
        measured_at: "2023-02-16T09:03:41.636Z",
        point_id: 131,
        location_id: 130,
        measure: "air_temp50",
        value: 2.3,
        unit: "°C",
    },
];
