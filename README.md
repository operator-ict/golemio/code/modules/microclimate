<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/microclimate</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/microclimate/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/microclimate/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/microclimate/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/microclimate/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/microclimate" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/microclimate">TypeDoc</a>
</p>
</div>

This module is intended for use with Golemio services. Refer [here](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/README.md) for further information on usage, local development and more.

## Installation

The APIs may be unstable. Therefore, we recommend to install this module as an exact version.

```bash
# Latest version
npm install --save-exact @golemio/microclimate@latest

# Development version
npm install --save-exact @golemio/microclimate@dev
```

<!-- ## Description -->

<!-- Insert module-specific info here -->

## Processing of historical data

To trigger the measurement data processing of a specific sensor for a specific period, use the queue in RabbitMQ `...microclimate.refreshMeasurementsById` according to the template:

```
{
    "sensorAddr": "323434316F317A18",
    "dateRange": "currentYear"
}
```

Where:

`sensorAddr` - Represents the transmitter's unique identifier. (Table `sensor_devices`, variable `address`)

`dateRange` - Time period: "today", "yesterday", "currentWeek", "previousWeek", "currentMonth", "previousMonth", "currentYear", "previousYear"

If an error is made in the RabbitMQ msg, the program will not crash, but will continue to work. It will also log an error of the type:

```
    error: {
        message:
            "[Queue ....microclimate.refreshMeasurementsById] Message validation failed: [dateRange must be one of the following values: today, yesterday, currentWeek, previousWeek, currentMonth, previousMonth, currentYear, previousYear]",
        class_name: "MessageDataValidator",
    }
```
